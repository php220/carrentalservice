import vue from '@vitejs/plugin-vue';
import laravel from 'laravel-vite-plugin';
import { defineConfig } from 'vite';

export default defineConfig({
    server: {
        host: '0.0.0.0',
        hmr: {
            host: 'localhost'
        }
    },
    plugins: [
        laravel({
            input: ['resources/js/app.js', 'resources/js/bootstrap.js'],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
});

// export default ({ command }) => ({
//     base: command === 'serve' ? '' : '/dist/',
//     publicDir: 'fake_dir_so_nothing_gets_copied',
//     build: {
//       manifest: true,
//       outDir: resolve(__dirname, 'public/dist'),
//       rollupOptions: {
//         input: 'resources/js/app.js',
//       },
//     },

//     plugins: [vue()],

//     resolve: {
//       alias: {
//         '@': resolve('./resources/js'),
//       },
//     },
//   });
