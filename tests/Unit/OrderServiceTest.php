<?php

namespace Tests\Unit;

use App\Repositories\OrderRepository;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Mockery;

class OrderServiceTest extends TestCase
{

    public $repo;
    public $service;
    private array $testOrders =
        [
            [
                'id' => '1',
                'user_id' => '1',
                'car_id' => '1',
                'status' => 'approve',
                'sum' => '10000',
                'number_of_days' => '10'
            ],
            [
                'id' => '2',
                'user_id' => '2',
                'car_id' => '2',
                'status' => 'approve',
                'sum' => '25000',
                'number_of_days' => '10'
            ],
        ];
    public bool $isEmpty = false;
    public int $expectedNumberOfOrders = 4;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->repo = Mockery::mock(OrderRepository::class);
        $this->service = new OrderService($this->mockOrderRepository());
    }

    public function mockOrderRepository()
    {
        $this->repo->shouldReceive('newOrdersDuringThePeriodWithPagination')->andReturnUsing(
            function ($start, $stop) {
                if (
                    (empty($start) || empty($stop))
                    || ($start > Carbon::now() || $stop > Carbon::now())
                    || $start > $stop
                ) {

                    return "The date is not correct";
                }

                return new Collection($this->testOrders);
            }
        );

        $this->repo->shouldReceive('countNewOrdersDuringThePeriod')->andReturnUsing(
            function ($start, $stop) {
                if (
                    (empty($start) || empty($stop))
                    || ($start > Carbon::now() || $stop > Carbon::now())
                    || $start > $stop
                ) {

                    return "The date is not correct";
                }

                return $this->expectedNumberOfOrders;
            }
        );

        $this->repo->shouldReceive('newOrdersDuringThePeriodWithPagination')->andReturnUsing(
            function ($start, $stop) {
                if (
                    (empty($start) || empty($stop))
                    || ($start > Carbon::now() || $stop > Carbon::now())
                    || $start > $stop
                ) {

                    return "The date is not correct";
                }

                return new Collection($this->testOrders);
            }
        );

        $this->repo->shouldReceive('getActiveOrders')->andReturnUsing(
            function () {
                if ($this->isEmpty === true) {
                    return new Collection();
                }

                return new Collection($this->testOrders);
            }
        );

        $this->repo->shouldReceive('orderByUserOrderData')
            ->andReturnUsing(
                function ($column, $keyword, $id){
                    if (
                        (empty($column) || empty($keyword) || empty($id))
                        || !in_array($keyword, ["asc", "desc"])
                        || !in_array($column, $this->service->columnForSorting)
                    ) {

                        return "Sorting error";
                    }

                    return new Collection($this->testOrders);
                }
            );

        return $this->repo;
    }

    /**
     * Data for the following tests: testNewOrdersDuringThePeriodSuccess, testCountNewOrdersDuringThePeriodSuccess
     *
     * @return array
     */
    public static function newOrdersDuringThePeriodSuccess(): array
    {
        return [
            ['2023-04-01', '2023-04-10'],
            ['2023-04-01', '2023-04-12'],
            ['2023-03-10', '2023-04-10'],
        ];
    }

    /**
     * Data for the following tests: testNewOrdersDuringThePeriodIfError, testCountNewOrdersDuringThePeriodIfError
     *
     * @return array
     */
    public static function newOrdersDuringThePeriodIfError(): array
    {
        return [
            ['2023-01-01', ''],
            ['', '2023-04-10'],
            [(string)Carbon::now()->addDays(5), '2023-04-13'],
            ['2023-04-01', (string)Carbon::now()->addDays(5)],
            ['2023-05-10', '2023-04-10'],
        ];
    }

    /**
     * Data for the following tests: testOrderByUserOrderDataSuccess
     *
     * @return array
     */
    public static function orderByUserOrderDataSuccess(): array
    {
        return [
            ['status', 'ASC', 1],
            ['number_of_days', 'DESC', 2],
            ['sum', 'DESC', 3],
        ];
    }

    /**
     * Data for the following tests: testOrderByUserOrderDataIfError
     *
     * @return array
     */
    public static function orderByUserOrderIfError(): array
    {
        return [
            ['', 'DESC', 1],
            ['id', '', 1],
            ['id', 'DESC', 10],
            ['sum', '111', 222],
            ['de', 'de', 1],
        ];
    }

    /**
     * Test newOrdersDuringThePeriodWithPagination success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::newOrdersDuringThePeriodWithPagination()
     */
    #[DataProvider('newOrdersDuringThePeriodSuccess')]
    public function testNewOrdersDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->newOrdersDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testOrders), $testResult);
    }

    /**
     * Test newOrdersDuringThePeriodWithPagination if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::newOrdersDuringThePeriodWithPagination()
     */
    #[DataProvider('newOrdersDuringThePeriodIfError')]
    public function testNewOrdersDuringThePeriodIfErrors(string $start, string $end)
    {
        $testResult = $this->service->newOrdersDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("The date is not correct", $testResult);
    }

    /**
     * Test countNewOrdersDuringThePeriod success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::countNewOrdersDuringThePeriod()
     */
    #[DataProvider('newOrdersDuringThePeriodSuccess')]
    public function testCountNewOrdersDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->countNewOrdersDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsInt($testResult);
        $this->assertEquals($this->expectedNumberOfOrders, $testResult);
    }

    /**
     * Test countNewOrdersDuringThePeriod if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::countNewOrdersDuringThePeriod()
     */
    #[DataProvider('newOrdersDuringThePeriodIfError')]
    public function testCountNewOrdersDuringThePeriodIfError(string $start, string $end)
    {
        $testResult = $this->service->countNewOrdersDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals('The date is not correct', $testResult);
    }

    /**
     * Test newOrdersProfit success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::newOrdersProfit()
     */
    #[DataProvider('newOrdersDuringThePeriodSuccess')]
    public function testNewOrdersProfitSuccess(string $start, string $end)
    {
        $expectedProfit = array_sum(array_column($this->testOrders, 'sum'));

        $testResult = $this->service->newOrdersProfit($start, $end);

        $this->assertIsInt($testResult);
        $this->assertEquals($expectedProfit, $testResult);
    }

    /**
     * Test newOrdersProfit if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\OrderService::newOrdersProfit()
     */
    #[DataProvider('newOrdersDuringThePeriodIfError')]
    public function testNewOrdersProfitIfError(string $start, string $end)
    {
        $testResult = $this->service->newOrdersProfit($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals('The date is not correct', $testResult);
    }

    /**
     * Test getActiveOrders success
     *
     * @return void
     * @covers \App\Services\OrderService::getActiveOrders()
     */
    public function testGetActiveOrdersSuccess()
    {
        $testResult = $this->service->getActiveOrders();

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testOrders), $testResult);
    }

    /**
     * Test getActiveOrders if error
     *
     * @return void
     * @covers \App\Services\OrderService::getActiveOrders()
     */
    public function testGetActiveOrdersIfError()
    {
        $this->isEmpty = true;

        $testResult = $this->service->getActiveOrders();

        $this->assertEmpty($testResult);
        $this->assertEquals(new Collection(), $testResult);
    }

    /**
     * Test orderByUserOrderData success
     *
     * @param string $column
     * @param string $keyword
     * @param int $id
     * @return void
     * @covers \App\Services\OrderService::orderByUserOrderData()
     */
    #[DataProvider('orderByUserOrderDataSuccess')]
    public function testOrderByUserOrderDataSuccess(string $column, string $keyword, int $id)
    {
        $testResult = $this->service->orderByUserOrderData($column, $keyword, $id);

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testOrders), $testResult);
    }

    /**
     * Test orderByUserOrderData if error
     *
     * @param string $column
     * @param string $keyword
     * @param int $id
     * @return void
     * @covers \App\Services\OrderService::orderByUserOrderData()
     */
    #[DataProvider('orderByUserOrderIfError')]
    public function testOrderByUserOrderDataIfError(string $column, string $keyword, int $id)
    {
        $testResult = $this->service->orderByUserOrderData($column, $keyword, $id);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("Sorting error", $testResult);
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
