<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Mockery;
use App\Repositories\UserRepository;
use App\Services\UserService;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class UserServiceTest extends TestCase
{
    public $repo;
    public $service;
    private array $testUser =
        [
            'id' => '1',
            'name' => 'Test',
            'email' => 'test@test.test',
            'type' => 'test',
            'is_active' => '1',
            'password' => '********',
        ];
    public bool $isEmpty = false;
    public int $expectedNumberOfActiveUsers = 50;
    public int $expectedNumberOfUsersDuringThePeriod = 50;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->repo = Mockery::mock(UserRepository::class);
        $this->service = new UserService($this->mockUserRepository());
    }

    public function mockUserRepository()
    {
        $this->repo->shouldReceive('activeUsersCount')->andReturnUsing(
            function () {
                if ($this->isEmpty === true) {
                    return 0;
                }

                return $this->expectedNumberOfActiveUsers;
            }
        );

        $this->repo->shouldReceive('showUsers')->andReturnUsing(
            function () {
                if ($this->isEmpty === true) {
                    return new Collection();
                }

                return new Collection($this->testUser);
            }
        );

        $this->repo->shouldReceive('newUsersDuringThePeriodWithPagination')->andReturnUsing(
            function ($start, $stop) {
                if (
                    (empty($start) || empty($stop))
                    || ($start > Carbon::now() || $stop > Carbon::now())
                    || $start > $stop
                ) {

                    return "The date is not correct";
                }

                return new Collection($this->testUser);
            }
        );

        $this->repo->shouldReceive('countNewUsersDuringThePeriod')->andReturnUsing(
            function ($start, $stop) {
                if (
                    (empty($start) || empty($stop))
                    || ($start > Carbon::now() || $stop > Carbon::now())
                    || $start > $stop
                ) {

                    return "The date is not correct";
                }

                return $this->expectedNumberOfUsersDuringThePeriod;
            }
        );

        return $this->repo;
    }

    /**
     * Data for the following tests: testNewUsersDuringThePeriodSuccess, testCountNewUsersDuringThePeriodSuccess
     *
     * @return array
     */
    public static function newUsersDuringThePeriodSuccess(): array
    {
        return [
            ['2023-04-07', '2023-04-10'],
            ['2023-04-01', '2023-04-12'],
            ['2023-03-10', '2023-04-10'],
        ];
    }

    /**
     * Data for the following tests: testNewUsersDuringThePeriodIfErrors, testCountNewUsersDuringThePeriodIfErrors
     *
     * @return array
     */
    public static function newUsersDuringThePeriodIfError(): array
    {
        return [
            ['2023-01-01', ''],
            ['', '2023-04-10'],
            [(string)Carbon::now()->addDays(5), '2023-04-13'],
            ['2023-04-01', (string)Carbon::now()->addDays(5)],
            ['2023-05-10', '2023-04-10'],
        ];
    }

    /**
     * Test for activeUsersCount success
     *
     * @return void
     * @covers \App\Services\UserService::activeUsersCount()
     */
    public function testActiveUsersCountSuccess()
    {
        $testResult = $this->service->activeUsersCount();

        $this->assertIsInt($testResult);
        $this->assertEquals($this->expectedNumberOfActiveUsers, $testResult);
    }

    /**
     * Test for activeUsersCount if error
     *
     * @return void
     * @covers \App\Services\UserService::activeUsersCount()
     */
    public function testActiveUsersCountIfError()
    {
        $this->isEmpty = true;

        $testResult = $this->service->activeUsersCount();

        $this->assertIsInt($testResult);
        $this->assertEquals(0, $testResult);
    }

    /**
     * Test for showUsers success
     *
     * @return void
     * @covers \App\Services\UserService::showUsers()
     */
    public function testShowUsersSuccess()
    {
        $testResult = $this->service->showUsers();

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testUser), $testResult);
    }

    /**
     * Test for showUsers if error
     *
     * @return void
     * @covers \App\Services\UserService::showUsers()
     */
    public function testShowUsersIfError()
    {
        $this->isEmpty = true;

        $testResult = $this->service->showUsers();

        $this->assertEmpty($testResult);
        $this->assertEquals(new Collection(), $testResult);
    }

    /**
     * Test newUsersDuringThePeriodWithPagination success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\UserService::newUsersDuringThePeriodWithPagination()
     */
    #[DataProvider('newUsersDuringThePeriodSuccess')]
    public function testNewUsersDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->newUsersDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testUser), $testResult);
    }

    /**
     * Test newUsersDuringThePeriodWithPagination if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\UserService::countNewUsersDuringThePeriod()
     */
    #[DataProvider('newUsersDuringThePeriodIfError')]
    public function testNewUsersDuringThePeriodIfErrors(string $start, string $end)
    {
        $testResult = $this->service->newUsersDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("The date is not correct", $testResult);
    }

    /**
     * Test countNewUsersDuringThePeriod success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\UserService::countNewUsersDuringThePeriod()
     */
    #[DataProvider('newUsersDuringThePeriodSuccess')]
    public function testCountNewUsersDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->countNewUsersDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsInt($testResult);
        $this->assertEquals($this->expectedNumberOfUsersDuringThePeriod, $testResult);
    }

    /**
     * Test countNewUsersDuringThePeriod if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\UserService::countNewUsersDuringThePeriod()
     */
    #[DataProvider('newUsersDuringThePeriodIfError')]
    public function testCountNewUsersDuringThePeriodIfErrors(string $start, string $end)
    {
        $testResult = $this->service->countNewUsersDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("The date is not correct", $testResult);
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
