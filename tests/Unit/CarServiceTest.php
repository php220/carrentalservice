<?php

namespace Tests\Unit;

use App\Repositories\CarRepository;
use App\Services\CarService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\TestCase;
use Mockery;
use PHPUnit\Framework\Attributes\DataProvider;

class CarServiceTest extends TestCase
{
    public $repo;
    public $service;
    private array $testCar =
        [
            'id' => '1',
            'brand' => 'Audi',
            'model' => 'TT',
            'type' => 'qqq',
            'box_type' => 'automatic',
            'fuel_type' => 'benzin',
            'seating' => '2',
            'price_per_days' => '1000',
        ];
    public int $expectedNumberOfCars = 25;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->repo = Mockery::mock(CarRepository::class);
        $this->service = new CarService($this->mockCarRepository());
    }

    public function mockCarRepository()
    {
        $this->repo->shouldReceive('newCarsDuringThePeriodWithPagination')
            ->andReturnUsing(
                function ($start, $stop){
                    if (
                        (empty($start) || empty($stop))
                        || ($start > Carbon::now() || $stop > Carbon::now())
                        || $start > $stop
                    ) {

                        return "The date is not correct";
                    }

                    return new Collection($this->testCar);
                }
            );

        $this->repo->shouldReceive('countNewCarsDuringThePeriod')
            ->andReturnUsing(
                function ($start, $stop){
                    if (
                        (empty($start) || empty($stop))
                        || ($start > Carbon::now() || $stop > Carbon::now())
                        || $start > $stop
                    ) {

                        return "The date is not correct";
                    }

                    return $this->expectedNumberOfCars;
                }
            );

        $this->repo->shouldReceive('orderByCarsData')
            ->andReturnUsing(
                function ($column, $keyword){
                    if (
                        (empty($column) || empty($keyword))
                        || !in_array($keyword, ["asc", "desc"])
                        || !in_array($column, $this->service->columnForSorting)
                    ) {

                        return "Sorting error";
                    }

                    return new Collection($this->testCar);
                }
            );

        return $this->repo;
    }

    /**
     * Data for the following tests: testNewCarsDuringThePeriodSuccess, testCountNewCarsDuringThePeriodSuccess
     *
     * @return array
     */
    public static function newCarsDuringThePeriodSuccess(): array
    {
        return [
            ['2023-04-07', '2023-04-10'],
            ['2023-04-01', '2023-04-12'],
            ['2023-03-10', '2023-04-10'],
        ];
    }

    /**
     * Data for the following tests: testNewCarsDuringThePeriodIfErrors, testCountNewCarsDuringThePeriodIfErrors
     *
     * @return array
     */
    public static function newCarsDuringThePeriodIfError(): array
    {
        return [
            ['2023-01-01', ''],
            ['', '2023-04-10'],
            [(string)Carbon::now()->addDays(5), '2023-04-13'],
            ['2023-04-01', (string)Carbon::now()->addDays(5)],
            ['2023-05-10', '2023-04-10'],
        ];
    }

    /**
     * Data for the following tests: testOrderByCarsDataSuccess
     *
     * @return array
     */
    public static function orderByCarsDataSuccess(): array
    {
        return [
            ['brand', 'ASC'],
            ['model', 'DESC'],
            ['type', 'DESC'],
        ];
    }

    /**
     * Data for the following tests: testOrderByCarsDataIfError
     *
     * @return array
     */
    public static function orderByCarsDataIfError(): array
    {
        return [
            ['', 'DESC'],
            ['id', ''],
            ['id', 'DESC'],
            ['sum', '111'],
            ['de', 'de'],
        ];
    }

    /**
     * Test newCarsDuringThePeriodWithPagination success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\CarService::newCarsDuringThePeriodWithPagination()
     */
    #[DataProvider('newCarsDuringThePeriodSuccess')]
    public function testNewCarsDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->newCarsDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testCar), $testResult);
    }

    /**
     * Test newCarsDuringThePeriodWithPagination if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\CarService::newCarsDuringThePeriodWithPagination()
     */
    #[DataProvider('newCarsDuringThePeriodIfError')]
    public function testNewCarsDuringThePeriodIfErrors(string $start, string $end)
    {
        $testResult = $this->service->newCarsDuringThePeriodWithPagination($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("The date is not correct", $testResult);
    }

    /**
     * Test CountNewCarsDuringThePeriod success
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\CarService::countNewCarsDuringThePeriod()
     */
    #[DataProvider('newCarsDuringThePeriodSuccess')]
    public function testCountNewCarsDuringThePeriodSuccess(string $start, string $end)
    {
        $testResult = $this->service->countNewCarsDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsInt($testResult);
        $this->assertEquals($this->expectedNumberOfCars, $testResult);
    }

    /**
     * Test CountNewCarsDuringThePeriod if error
     *
     * @param string $start
     * @param string $end
     * @return void
     * @covers \App\Services\CarService::countNewCarsDuringThePeriod()
     */
    #[DataProvider('newCarsDuringThePeriodIfError')]
    public function testCountNewCarsDuringThePeriodIfErrors(string $start, string $end)
    {
        $testResult = $this->service->countNewCarsDuringThePeriod($start, $end);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("The date is not correct", $testResult);
    }

    /**
     * Test orderByCarsData success
     *
     * @param string $column
     * @param string $keyword
     * @return void
     * @covers \App\Services\CarService::orderByCarsData()
     */
    #[DataProvider('orderByCarsDataSuccess')]
    public function testOrderByCarsDataSuccess(string $column, string $keyword)
    {
        $testResult = $this->service->orderByCarsData($column, $keyword);

        $this->assertNotEmpty($testResult);
        $this->assertEquals(new Collection($this->testCar), $testResult);
    }

    /**
     * Test orderByCarsData if error
     *
     * @param string $column
     * @param string $keyword
     * @return void
     * @covers \App\Services\CarService::orderByCarsData()
     */
    #[DataProvider('orderByCarsDataIfError')]
    public function testOrderByCarsDataIfError(string $column, string $keyword)
    {
        $testResult = $this->service->orderByCarsData($column, $keyword);

        $this->assertNotEmpty($testResult);
        $this->assertIsString($testResult);
        $this->assertEquals("Sorting error", $testResult);
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
