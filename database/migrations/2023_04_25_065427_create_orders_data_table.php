<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders_data', function (Blueprint $table) {
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('car_id')->unsigned();
            $table->unsignedDecimal('sum', $precision = 7, $scale = 2);
            $table->unsignedTinyInteger('number_of_days');
            $table->boolean('is_active')->default(1);
            $table->timestamp('end_of_lease');
            $table->timestamps();
            $table->foreign("order_id")->references("id")->on("orders");
            $table->foreign("car_id")->references("id")->on("cars");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders_data');
    }
};
