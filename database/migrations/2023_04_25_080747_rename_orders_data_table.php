<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
         Schema::rename('orders_data', 'orderData');

         Schema::table('orderData', function (Blueprint $table) {
            $table->foreign("order_id")->references("id")->on("orders");
            $table->foreign("car_id")->references("id")->on("cars");
         });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
