<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('image_cars', function (Blueprint $table) {
            $table->mediumInteger('size')->unsigned()->after('default');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('image_cars', function (Blueprint $table) {
            $table->dropColumn('size');
        });
    }
};
