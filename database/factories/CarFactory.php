<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Enums\CarStatusEnum;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $this->faker->addProvider(new \Faker\Provider\Fakecar($this->faker));
        return [
            "brand" => $this->faker->vehicleBrand,
            "model" => $this->faker->vehicleModel,
            "type" => $this->faker->vehicleType,
            "box_type" => $this->faker->vehicleGearBoxType,
            "fuel_type" => $this->faker->vehicleFuelType,
            "seating" => $this->faker->vehicleSeatCount,
            "price_per_day" => $this->faker->numberBetween($min = 10, $max = 1000),
            "is_active" => 1
        ];
    }
}
