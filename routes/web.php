<?php

use App\Models\Car;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\CarController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\ManagerCarController;
use App\Http\Controllers\PayPalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login');
});

/**
 *
 * A group of routers responsible for the user side
 *
 */
Route::middleware(["auth:sanctum", "user.check", "disabled.check"])->group(function () {
    Route::get('/', [CarController::class, "index"])->name('dashboard');
    Route::get('car/search', [CarController::class, "search"])->name('searchCar');
    Route::get('order/search', [OrderController::class, "search"])->name('searchOrder');
    Route::get('/success', [PayPalController::class, 'success']);
    Route::get('/error', [PayPalController::class, 'error']);

    Route::prefix('orders')->group(
        function () {
            Route::get('/{user}', [OrderController::class, "index"])->name('order');
            Route::get('/additionally/{order}', [OrderController::class, "ordersAdditionallyInfo"])->name('orderAdditionally');

            Route::post('/pay', [PayPalController::class, 'pay'])->name('pay');

            Route::post('/add/{user}', [OrderController::class, "addNew"])->name('addOrder');

            Route::get('/invoicepdf/{order}', [OrderController::class, 'invoicePdf'])
                ->name('orderInvoicePDF');

            Route::get('/delete/{order}', [OrderController::class, "delete"])->name('orderDelete');
        }
    );

    Route::prefix('profile')->group(
        function () {
            Route::get('/{user}', [UserController::class, 'profile'])->name('profile');
            Route::put('/update/{user}', [UserController::class, 'update'])->name('profile.update');
        }
    );
});

/**
 *
 * A group of routers responsible for the manager side
 *
 */
Route::middleware(['auth:sanctum', 'admin.check', "disabled.check"])->group(function () {
    Route::prefix('manager')->group(
        function () {
            Route::get('/', [ManagerController::class, 'index'])->name('manager');

            Route::get('/car/search', [ManagerController::class, 'searchCar'])->name('carSearch');
            Route::get('/order/search', [ManagerController::class, 'searchOrder'])->name('orderSearch');
            Route::get('/user/search', [ManagerController::class, 'searchUser'])->name('userSearch');

            Route::post('/car/update/status/indataorder', [ManagerController::class, 'updateCarStatusInOrderData'])
                ->name('carUpdateStatusInOrderData');

            Route::get('/sorting', [ManagerController::class, 'sorting'])->name('carSorting');

            Route::get('/users', [ManagerController::class, 'displayAllUsers'])->name('manager.users');
            Route::post('/users/edit', [ManagerController::class, 'editStatusUser'])
                ->name('editStatusUser');

            Route::get('/orders', [ManagerController::class, 'displayAllOrders'])->name('manager.orders');
            Route::post('/orders/edit', [ManagerController::class, 'editStatusOrder'])
                ->name('manager.editOrder');

            Route::get('/report', [ManagerController::class, 'report'])->name('report');
            Route::get('/report/data', [ManagerController::class, 'reportData'])->name('reportData');
            Route::get('/report/additiondata', [ManagerController::class, 'reportAdditionData'])
                ->name('manager.reportAddition');
            Route::post('/report/export', [ManagerController::class, 'reportExportExcel'])
                ->name('manager.reportExportExcel');

            Route::post('/car/editstatus', [ManagerController::class, 'editStatusCar'])
                ->name('manager.editCar');
            Route::resource('/car', ManagerCarController::class);
            Route::post('/car2/{car}', [ManagerCarController::class, 'update'])
                ->name('manager.reportExportExcel');

        }
    );
});

