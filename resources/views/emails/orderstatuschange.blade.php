<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>Hello, {{$user->name}} the status of your order has been changed to <b>{{$order->status}}</b> @if($order->status === 'approve') go to the order page for payment @endif.</p>
</body>
</html>
