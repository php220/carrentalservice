<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>User "{{$user->email}}" wants to order these cars: </p>

@foreach ($order->subOrder()->get() as $subOrder)

<hr>
<table style="width: 100%">
    <tr>
        <td>
            <strong>Brand</strong>
        </td>
        <td>
            <strong>Model</strong>
        </td>
        <td>
            <strong>Type</strong>
        </td>
        <td>
            <strong>Box Type</strong>
        </td>
        <td>
            <strong>Fuel Type</strong>
        </td>
        <td>
            <strong>Number Of Days</strong>
        </td>
        <td>
            <strong>Total amount</strong>
        </td>
    </tr>

        <tr>
            <td>{{ $subOrder->car()->first()->brand }}</td>
            <td>{{ $subOrder->car()->first()->model }}</td>
            <td>{{ $subOrder->car()->first()->type }}</td>
            <td>{{ $subOrder->car()->first()->box_type }}</td>
            <td>{{ $subOrder->car()->first()->fuel_type }}</td>
            <td>{{ $subOrder->number_of_days }}</td>
            <td>{{ $subOrder->sum }}</td>
        </tr>

</table>

@endforeach
</body>
</html>
