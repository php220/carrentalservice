<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>Hello, {{ $user->name }}, less than 24 hours left until the lease expires <b>{{$order->car()->first()->brand . ' ' .  $order->car()->first()->model}}</b> rental period expires, during this time, please return the car to the dealer where you picked it up.</p>
</body>
</html>
