@extends('layouts.app')

@section('content')

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="" action="{{route('order.delete')}}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Order</h5>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to cancel your rental car?</p>
                        <input type="hidden" name="deleteOrder" id="orderId">
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('order', ['user' => Auth::user()]) }}"
                           class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </a>
                        <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(isset($ordersSearch))

        @include('search.ordersearch')
        @include('partials.sorting')
        @include('partials.partials')
        <h2>Orders</h2>
        <div id="orders">
            @foreach($ordersSearch as $order)

                <section class="content">

                    @include('orders.order')

                </section>
            @endforeach
        </div>
        {{ $ordersSearch->links() }}
    @endif
    @if (isset($orders) && !$orders->count())

        <div class="d-flex justify-content-center">
            <h3>You have no orders</h3>
        </div>

    @elseif(isset($orders))

        @include('search.ordersearch')
        @include('partials.sorting')
        @include('partials.partials')
        <h2>Orders</h2>
        <div id="orders">
            @foreach($orders as $order)

                <section class="content">

                    @include('orders.order')

                </section>
            @endforeach
        </div>
        {{ $orders->links() }}

    @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script>
        if (document.addEventListener) {
            document.addEventListener("focusout", function (event) {
                const data = event.target.value;
                sorting(data)
            });

            function sorting(data) {
                $("#orders").html();
                $(".pagination").remove();
                $.ajax({
                    method: "POST",
                    url: "{{ route('order.sorting', ['user' => Auth::id()]) }}",
                    data: {"_token": "{{ csrf_token() }}", data}
                }).done(function (msg) {
                    $("#orders").html(msg);
                });

            }
        }
        $("#delete-form").on('click', function (event) {
            event.preventDefault();
            let orderId = $(this).val();
            $('#orderId').val(orderId);
            $('#deleteModal').modal('show');
        });
    </script>

@endsection
