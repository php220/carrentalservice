<div class="card mb-2">
    <div class="card-header">
        <h3 class="card-title">Order #{{$order->id}}</h3>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
            <tr>
                <th style="width: 20%">
                    Car Brand
                </th>
                <th style="width: 15%">
                    Number of days
                </th>
                <th style="width: 15%">
                    Total amount
                </th>
                <th style="width: 15%">
                    End of lease
                </th>
                <th style="width: 8%" class="text-center">
                    Status
                </th>
                <th style="width: 20%">

                </th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <h5>
                        {{$order->car()->first()->brand}}
                    </h5>

                    <small>
                        Model: {{$order->car()->first()->model}}
                    </small>
                </td>
                <td>
                    <ul>
                        {{$order->number_of_days}}
                    </ul>
                </td>
                <td>
                    {{$order->sum}}
                </td>
                <td>
                    {{$order->end_of_lease}}
                </td>
                <td>
                    <span class="d-flex justify-content-center">{{$order->status}}</span>
                </td>
                <td class="project-actions text-right">
                    <a href="{{ route('order.invoicepdf', ['order' => $order->id]) }}"
                       class="btn btn-primary float-right mb-1" style="margin-right: 5px;">
                        Generate PDF
                    </a>
                    @if($order->status->value != 'approve' && $order->status->value != 'cancel')

                        <button id="delete-form" type="button" class="btn btn-danger mb-1" value="{{$order->id}}">
                            Сancellation of order
                        </button>

                    @else
                        <p>You cannot cancel a confirmed order</p>
                    @endif
                </td>
            </tr>
            </tbody>

        </table>
    </div>
</div>
