@extends('layouts.app')

@section('content')
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container-thank {
            margin-top: 50px;
            text-align: center;
        }
        .icon {
            font-size: 6rem;
            color: #28a745;
        }
    </style>
    <div class="container-thank">
        <i class="fas fa-check-circle icon"></i>
        <h1>Thank You!</h1>
        <p>An email was sent to you with further instructions</p>
    </div>
    <script src="https://kit.fontawesome.com/c8e4d183c2.js" crossorigin="anonymous"></script>
    <script>
        setTimeout(function() {
            window.location.href = '{{route('car')}}'
        }, 5000);
    </script>
@endsection

