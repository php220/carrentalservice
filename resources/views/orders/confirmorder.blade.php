@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="invoice p-3 mb-3">

            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-globe"></i> Car Rental Service
                        <small class="float-centre">Date: {{ date('m/d/Y') }}</small>
                    </h4>
                </div>

            </div>

            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    From
                    <address>
                        <strong>Manager</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Email: carrentalservice@example.com
                    </address>
                </div>

                <div class="col-sm-4 invoice-col">
                    To
                    <address>
                        <strong>{{$user->name}}</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Email: {{$user->email}}
                    </address>
                </div>

            </div>


            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Car name</th>
                            <th>Fuel type</th>
                            <th>Number of days</th>
                            <th>Price per day</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $car->brand . " " . $car->model . "( " . $car->type . " )"}}</td>
                            <td>{{$car->fuel_type}}</td>
                            <td>{{$numberOfDays}}</td>
                            <td>{{$car->price_per_day}}</td>
                            <td>$@convert($sum)</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="row no-print">
                <div class="col-12">
                    <!--<a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i
                            class="fas fa-print"></i> Print</a>-->
                    <form method="POST" action="{{route('confirmEmail', ['user' => Auth::id(), 'car' => $car->id])}}">
                        <input type="hidden" name="sum" value="{{ $sum }}">
                        <input type="hidden" name="numberOfDays" value="{{$numberOfDays}}">
                        <button class="btn btn-success">Confirm</button>
                        @csrf
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
