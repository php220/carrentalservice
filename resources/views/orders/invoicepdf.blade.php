<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
</head>
<body>
<h2>Car Rental Service</h2>
<h3>Date: {{ date('m/d/Y') }}</h3>
<table style="width: 100%">
    <tr>
        <td>
            From
        </td>
        <td>
            To
        </td>
        <td>
            Invoice #{{ $order->id }}
        </td>
    </tr>
    <tr>
        <td>
            Car Rental Service
        </td>
        <td>
            <strong>{{ $user->name }}</strong>
        </td>
        <td>

        </td>
    </tr>
    <tr>
        <td>
            795 Folsom Ave, Suite 600
        </td>
        <td>
            Email: {{ $user->email }}
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: carrentalservice@example.com
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
</table>
<br>
<hr>

@foreach ($order->subOrder()->get() as $subOrder)
<table style="width: 100%">
    <tr>
        <td>
            <strong>Brand</strong>
        </td>
        <td>
            <strong>Model</strong>
        </td>
        <td>
            <strong>Type</strong>
        </td>
        <td>
            <strong>Box Type</strong>
        </td>
        <td>
            <strong>Fuel Type</strong>
        </td>
        <td>
            <strong>Number Of Days</strong>
        </td>
        <td>
            <strong>Total amount</strong>
        </td>
    </tr>

        <tr>
            <td>{{ $subOrder->car()->first()->brand }}</td>
            <td>{{ $subOrder->car()->first()->model }}</td>
            <td>{{ $subOrder->car()->first()->type }}</td>
            <td>{{ $subOrder->car()->first()->box_type }}</td>
            <td>{{ $subOrder->car()->first()->fuel_type }}</td>
            <td>{{ $subOrder->number_of_days }}</td>
            <td>{{ $subOrder->sum }}</td>
        </tr>

</table>
<hr>
@endforeach

<table style="width: 100%;">
    <tr>
        <td>
            <strong>Amount Due {{ date('m/d/Y') }}</strong>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Subtotal:</strong> $@convert($totalPrice)
        </td>
    </tr>
    <tr>
        <td>
            <strong>Shipping:</strong> $0
        </td>
    </tr>
    <tr>
        <td>
            <strong>Total:</strong> $@convert($totalPrice)
        </td>
    </tr>
</table>

</body>
</html>

