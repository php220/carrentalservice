@extends('layouts.app')

@section('content')
    <div class="container-fluid">
    @if(isset($carsSearch))

            @include('search.carsearch')
            <div class="row row-cols-3">

                @foreach($carsSearch as $car)
                    @include('cars.cars', ["car" => $car])
                @endforeach

            </div>

        @else

            @include('search.carsearch')
            @include('partials.carsorting')
            <div id="cars" class="row row-cols-3">

                @foreach($cars as $car)
                    @include('cars.cars', ["car" => $car])
                @endforeach

            </div>
            {{ $cars->links() }}

        {{-- @endif
    </div>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script type="text/javascript">
        if (document.addEventListener) {
            document.addEventListener("keyup", function (event) {
                const data = {
                    days: event.target.value,
                    id: event.target.id
                }
                let pricePerDay = document.getElementById('pricePerDay' + data.id).textContent;
                document.getElementById('sum' + data.id).textContent = pricePerDay * data.days;
            });
        }

        $("#sorting-car").on('focusout', function (event) {
            const sortingData = event.target.value;
            sorting(sortingData)
        });

        function sorting(sortingData) {
            $("#cars").html();
            $(".pagination").remove();
            $.ajax({
                method: "POST",
                url: "{{route('car.sorting')}}",
                data: {"_token": "{{ csrf_token() }}", sortingData}
            }).done(function (msg) {
                $("#cars").html(msg);
            });


        }
    </script> --}}
@endsection
