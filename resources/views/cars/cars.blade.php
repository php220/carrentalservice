<div class="col-md-4">
    <div class="card mb-4 shadow-sm">
        {{-- <form method="post" action="{{route('order.confirm', ['user' => Auth::id(), 'car' => $car->id])}}">
            @csrf
            @if($car->is_active === 0)

                @include('partials.car_in_lease')

            @else --}}
                <div class="card-body">
                    <h5 class="card-text">{{ $car->brand . " " . $car->model . "( " . $car->type . " )"}}</h5>
                    <span class="card-text">Box type: {{$car->box_type}}</span>
                    <span class="card-text float-lg-end">Fuel type: {{$car->fuel_type}}</span>

                    <div class="d-flex justify-content-between align-items-center">
                        <span class="text-muted">
                            Price per day: $<span id="pricePerDay{{$car->id}}">
                                {{$car->price_per_day}}
                            </span>
                        </span>
                        <span  class="text-muted">
                            Sum: $<span id="sum{{$car->id}}">
                                @convert(0)
                            </span>
                        </span>
                    </div>

                    <label for="{{$car->id}}">Number of days</label>
                    <input id="{{$car->id}}"
                           class="form-control w-25 @error("numberOfDays.$car->id") is-invalid @enderror"
                           name="numberOfDays[{{$car->id}}]" value="">
                    @error("numberOfDays.$car->id")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input type="submit" class="btn btn-success mt-2" value="Order">
                </div>
            {{-- @endif
        </form> --}}
    </div>
</div>
