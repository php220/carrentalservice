<div class="card-body" style="background-color: #adb5bd;">
    <h5 class="card-text">{{ $car->brand . " " . $car->model . "( " . $car->type . " )"}}</h5>
    <span class="card-text">Box type: {{$car->box_type}}</span>
    <span class="card-text float-lg-end">Fuel type: {{$car->fuel_type}}</span>

    <div class="d-flex justify-content-between align-items-center">
        <span>
            Price per day: $<span id="pricePerDay{{$car->id}}">
                {{$car->price_per_day}}
            </span>
        </span>
        <span>
            Sum: $<span id="sum{{$car->id}}">
                @convert(0)
            </span>
        </span>
    </div>

    <label for="{{$car->id}}">Number of days</label>
    <input id="{{$car->id}}"
           class="form-control w-25"
           name="numberOfDays[{{$car->id}}]" style="background-color: #ced4da;" value="">
    @foreach($car->orders as $order)
        @if(in_array($order->status->value, ['new', 'approve']))
            <h5 class="mt-3">The car is leased to {{date('Y-m-d', strtotime($order->end_of_lease))}}</h5>
        @endif
    @endforeach
</div>
