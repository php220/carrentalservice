<div id="sorting-car" class="card card-primary mb-3">
    <div class="card-header">
        <h5 class="card-title">Sorting</h5>
    </div>
    <div class="row mb-2">
        <div class="col-md-2">
            <div class="form-group">
                <label for="brandSort">Brand</label>
                <select id="brandSort" class="form-control custom-select">
                    <option value="brand-ASC">Ascending</option>
                    <option value="brand-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label for="modelSort">Model</label>
                <select id="modelSort" class="form-control custom-select">
                    <option value="model-ASC">Ascending</option>
                    <option value="model-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label for="typeSort">Type</label>
                <select id="typeSort" class="form-control custom-select">
                    <option value="type-ASC">Ascending</option>
                    <option value="type-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label for="boxTypeSort">Box type</label>
                <select id="boxTypeSort" class="form-control custom-select">
                    <option value="box_type-ASC">Ascending</option>
                    <option value="box_type-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label for="fuelTypeSort">Fuel type</label>
                <select id="fuelTypeSort" class="form-control custom-select">
                    <option value="fuel_type-ASC">Ascending</option>
                    <option value="fuel_type-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label for="pricePerDaySort">Price per day</label>
                <select id="pricePerDaySort" class="form-control custom-select">
                    <option value="price_per_day-ASC">Ascending</option>
                    <option value="price_per_day-DESC">Decreasing</option>
                </select>
            </div>
        </div>
    </div>
</div>

