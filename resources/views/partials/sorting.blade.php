<div class="card card-primary mb-3">
    <div class="card-header">
        <h5 class="card-title">Sorting</h5>
    </div>
    <div class="row mb-2">
        <div class="col-md-3">
            <div class="form-group">
                <label for="numberOfDaysSort">Number of days</label>
                <select id="numberOfDaysSort" class="form-control custom-select">
                    <option value="number_of_days-ASC">Ascending</option>
                    <option value="number_of_days-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="totalAmountSort">Total amount</label>
                <select id="totalAmountSort" class="form-control custom-select">
                    <option value="sum-ASC">Ascending</option>
                    <option value="sum-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="endOfLeaseSort">End of lease</label>
                <select id="endOfLeaseSort" class="form-control custom-select">
                    <option value="end_of_lease-ASC">Ascending</option>
                    <option value="end_of_lease-DESC">Decreasing</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="statusSort">Status</label>
                <select id="statusSort" class="form-control custom-select">
                    <option value="status-ASC">Ascending</option>
                    <option value="status-DESC">Decreasing</option>
                </select>
            </div>
        </div>
    </div>
</div>

