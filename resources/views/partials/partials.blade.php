<div class="col-md-12 mt-3">
    @if( Session::has( 'success' ))
        <div class="alert alert-success alert-dismissible">
            <h5><i class="icon fas fa-check"></i> Alert!</h5>
            {{ Session::get( 'success' ) }}
        </div>
    @endif
</div>

