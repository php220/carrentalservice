<table id="cars" class="table table-bordered table-hover dataTable dtr-inline"
       aria-describedby="example2_info">
    <thead>
    <tr>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1"
            aria-label="Rendering engine: activate to sort column ascending"
            style="width: 10%">Id
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="Browser: activate to sort column ascending"
            style="width: 20%">Brand and Models
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="Platform(s): activate to sort column ascending"
            style="width: 10%">
            Status
        </th>
        <th class="sorting sorting_desc" tabindex="0" aria-controls="example2"
            rowspan="1" colspan="1"
            aria-label="Engine version: activate to sort column ascending"
            aria-sort="descending" style="width: 15%">Price per day
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($cars as $car)
        <tr>
            <td class="dtr-control" tabindex="0">{{$car->id}}</td>

            <td>{{$car->brand . ' ' .$car->model}}</td>

            <td>{{$car->is_active}}</td>

            <td class="sorting_1">$@convert($car->price_per_day)</td>
        </tr>
    @endforeach
    </tbody>
</table>
