<table class="table table-striped projects">
    <thead>
    <tr>
        <th style="width: 20%">
            New Users
        </th>
        <th style="width: 15%">
            New Cars
        </th>
        <th style="width: 15%">
            New Orders
        </th>
        <th style="width: 15%">
            Profit
        </th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>
            {{session('newUsers')}}
        </td>
        <td>
            {{session('newCars')}}
        </td>
        <td>
            {{session('newOrders')}}
        </td>
        <td>
            $@convert(session('profit'))
        </td>
    </tr>
    </tbody>
</table>
