<table id="orders" class="table table-bordered table-hover dataTable dtr-inline"
       aria-describedby="example2_info">
    <thead>
    <tr>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1"
            aria-label="Rendering engine: activate to sort column ascending"
        >Order id
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1"
            aria-label="Rendering engine: activate to sort column ascending"
        >Car id
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="Platform(s): activate to sort column ascending"
        >Number of days
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="CSS grade: activate to sort column ascending"
        >Sum
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
        colspan="1" aria-label="CSS grade: activate to sort column ascending"
        >Active
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
        colspan="1" aria-label="CSS grade: activate to sort column ascending"
        >End of lease
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
        colspan="1" aria-label="CSS grade: activate to sort column ascending"
        >Created at
</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td class="dtr-control" tabindex="0">{{$order->order_id}}</td>

            <td class="dtr-control" tabindex="0">{{$order->car_id}}</td>

            <td class="sorting_1">{{$order->number_of_days}}</td>

            <td>{{$order->sum}}</td>

            <td>{{$order->is_active}}</td>

            <td>{{$order->end_of_lease}}</td>

            <td>{{$order->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

