@extends('adminlte::page')

@section('content')
    <div id="alert_placeholder"></div>

    <h3 class="mb-2">
        Page for viewing statistics
    </h3>
    <form class="" action="{{ route('manager.reportData') }}" method="POST">
        @csrf
        <div class="form-group row">
            <div class="col-sm-6">
                <input id="date-report" class="form-control @error('startDate') is-invalid @enderror" name="startDate"
                       placeholder="Enter start date format YYYY-MM-DD"
                       value="{{ old('startDate' ?? '') }}">
                @error('startDate')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <input id="date-report" class="form-control @error('endDate') is-invalid @enderror" name="endDate"
                       placeholder="Enter end data format YYYY-MM-DD" value="{{ old('endDate' ?? '') }}">
                @error('endDate')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        @if(session('error'))
            <p style="color: red">{{ session('error') }}</p>
        @endif

        <button class="btn btn-success my-2 my-sm-0" type="submit">Get data</button>
    </form>

    @if(session('newUsers'))

        <h4 class="mt-2">
            <small class="text-muted">Sampling data for the period </small>from {{session('startingDate')}}
            to {{session('endingDate')}}
        </h4>
        <div class="card mt-2">
            <div class="card-body p-0">

                @include('manager.report.report_table')

            </div>
        </div>
        <div class="card mt-3">
            <div class="card-body">
                <h5 class="d-flex justify-content-center">
                    Additional information
                </h5>
                <h6>
                    Choose column
                </h6>
                <select name="information" id="information" class="form-control w-100">
                    <option value="default">...</option>
                    <option value="users">Users</option>
                    <option value="cars">Cars</option>
                    <option value="orders">Orders</option>
                </select>
            </div>
        </div>
        <div id="report-table">
            @include('partials.choose_additional_table')
        </div>

        @include('manager.report.report_excel_format')
    @endif
@stop

@section('js')
    <script>
        $("#information").on('change', function (event) {
            const data =
                {
                    table: event.target.value,
                    start: "{{session('startingDate')}}",
                    end: "{{session('endingDate')}}"
                }
            additionalInformation(data)
        });

        function additionalInformation(data) {
            $("#report-table").html();
            $.ajax({
                method: "GET",
                url: "{{ route('manager.reportAddition') }}",
                data: {"_token": "{{ csrf_token() }}", data}
            }).done(function (msg) {
                $("#report-table").html(msg);
            });

        }

        //let btn = $('#topPage');

        /*$(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });*/
    </script>
@stop
