<table id="report_table" class="table table-bordered table-hover dataTable dtr-inline"
       aria-describedby="example2_info">
    <thead>
    <tr>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1"
            aria-label="Rendering engine: activate to sort column ascending"
        >Id
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="Browser: activate to sort column ascending"
        >Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="Platform(s): activate to sort column ascending"
        >Email
        </th>
        <th class="sorting sorting_desc" tabindex="0" aria-controls="example2"
            rowspan="1" colspan="1"
            aria-label="Engine version: activate to sort column ascending"
            aria-sort="descending">Status
        </th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
            colspan="1" aria-label="CSS grade: activate to sort column ascending"
        >Created at
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td class="dtr-control" tabindex="0">{{$user->id}}</td>

            <td>{{$user->name}}</td>

            <td>{{$user->email}}</td>

            <td>{{$user->is_active}}</td>

            <td>{{$user->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

