<div class="card mt-3">
    <div class="card-body">
        <h5 class="d-flex justify-content-center">
            Get report excel
        </h5>

        <div class="row">
            <div class="col-sm-4">
                <a class="btn btn-success btn-block" href="{{ route('manager.reportExportExcel') }}"
                   onclick="event.preventDefault();
                    document.getElementById('exportUsersReport').submit();">
                    {{ __('Download Excel Users') }}
                </a>

                <form id="exportUsersReport" action="{{ route('manager.reportExportExcel') }}" method="POST"
                      class="d-none">
                    <input type="hidden" name="table" value="users">
                    <input type="hidden" name="start" value="{{session('startingDate')}}">
                    <input type="hidden" name="end" value="{{session('endingDate')}}">
                    @csrf
                </form>
            </div>

            <div class="col-4">
                <a class="btn btn-success btn-block" href="{{ route('manager.reportExportExcel') }}"
                   onclick="event.preventDefault();
                    document.getElementById('exportOrdersReport').submit();">
                    {{ __('Download Excel Orders') }}
                </a>

                <form id="exportOrdersReport" action="{{ route('manager.reportExportExcel') }}" method="POST"
                      class="d-none">
                    <input type="hidden" name="table" value="orders">
                    <input type="hidden" name="start" value="{{session('startingDate')}}">
                    <input type="hidden" name="end" value="{{session('endingDate')}}">
                    @csrf
                </form>
            </div>

            <div class="col-4">
                <a class="btn btn-success btn-block" href="{{ route('manager.reportExportExcel') }}"
                   onclick="event.preventDefault();
                    document.getElementById('exportCarsReport').submit();">
                    {{ __('Download Excel Cars') }}
                </a>

                <form id="exportCarsReport" action="{{ route('manager.reportExportExcel') }}" method="POST"
                      class="d-none">
                    <input type="hidden" name="table" value="cars">
                    <input type="hidden" name="start" value="{{session('startingDate')}}">
                    <input type="hidden" name="end" value="{{session('endingDate')}}">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
