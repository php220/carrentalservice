<div class="row">
    <div class="col-12 mt-3">
        <div class="card">
            @include('partials.partials')
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="users" class="table table-bordered table-hover dataTable dtr-inline"
                                   aria-describedby="example2_info">
                                <thead>
                                <tr>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1"
                                        aria-label="Rendering engine: activate to sort column ascending"
                                    >Id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Browser: activate to sort column ascending"
                                    >Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                    >Email
                                    </th>
                                    <th class="sorting sorting_desc" tabindex="0" aria-controls="example2"
                                        rowspan="1" colspan="1"
                                        aria-label="Engine version: activate to sort column ascending"
                                        aria-sort="descending">Status
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="CSS grade: activate to sort column ascending"
                                    >Created at
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="dtr-control" tabindex="0">{{$user->id}}</td>

                                        <td>{{$user->name}}</td>

                                        <td>{{$user->email}}</td>

                                        <td>
                                            <div class="report-change-user-status">
                                                <select name="is_active" id="is_active_{{$user->id}}"
                                                        class="form-control w-100">

                                                    <option @if($user->is_active) selected
                                                            @endif value="1">Active
                                                    </option>

                                                    <option @if(!$user->is_active) selected
                                                            @endif value="0">Disabled
                                                    </option>

                                                </select>
                                            </div>
                                        </td>

                                        <td>{{$user->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">Id</th>
                                    <th rowspan="1" colspan="1">Name</th>
                                    <th rowspan="1" colspan="1">Email</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Created at</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<script>
    $(function () {
        $('#users').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $(".report-change-user-status").on('change', function (event) {
        const data =
            {
                is_active: event.target.value,
                id: event.target.id,
            }

        statusUserChange(data)
    });

    function statusUserChange(data) {
        $("#" + data.id).html();
        $.ajax({
            method: "POST",
            url: "{{route('manager.editUser')}}",
            data: {"_token": "{{ csrf_token() }}", data}
        }).done(function (msg) {
            showalert("Status was update");
        });
    }


    function showalert(message) {

        $('#alert_placeholder').append(
            '<div id="alter-success" class="alert alert-success alert-dismissible fade show mt-2" role="alert">'
            + message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button></div>'
        );
        window.scrollTo(0, 0);
    }
</script>
