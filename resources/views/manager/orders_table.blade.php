<div class="row">
    <div class="col-12 mt-3">
        <div class="card">
            @include('partials.partials')
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="orders" class="table table-bordered table-hover dataTable dtr-inline"
                                   aria-describedby="example2_info">
                                <thead>
                                <tr>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1"
                                        aria-label="Rendering engine: activate to sort column ascending"
                                    >User id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1"
                                        aria-label="Rendering engine: activate to sort column ascending"
                                    >Car id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Browser: activate to sort column ascending"
                                    >Email
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                    >Number of days
                                    </th>
                                    <th class="sorting sorting_desc" tabindex="0" aria-controls="example2"
                                        rowspan="1" colspan="1"
                                        aria-label="Engine version: activate to sort column ascending"
                                        aria-sort="descending">Status
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="CSS grade: activate to sort column ascending"
                                    >Sum
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td class="dtr-control" tabindex="0">{{$order->user_id}}</td>

                                        <td class="dtr-control" tabindex="0">{{$order->car_id}}</td>

                                        <td>{{$order->user()->first()->email}}</td>

                                        <td class="sorting_1">{{$order->number_of_days}}</td>

                                        <td>
                                            <div class="report-change-order-status">

                                            <select name="status" id="status_{{$order->id}}" class="form-control w-100">
                                                @foreach(App\Enums\OrderStatusEnum::values() as $key=>$value)
                                                    <option @if($order->status->value == $value)
                                                                selected
                                                            @endif
                                                            value="{{ $value }}">{{ $key }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            </div>
                                        </td>

                                        <td>{{$order->sum}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">User id</th>
                                    <th rowspan="1" colspan="1">Car id</th>
                                    <th rowspan="1" colspan="1">Email</th>
                                    <th rowspan="1" colspan="1">Number of days</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Sum</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<script>
    $(function () {
        $('#orders').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $(".report-change-order-status").on('change', function (event) {
        const data = {
            status: event.target.value,
            id: event.target.id,
        }
        statusOrderChange(data)
    });

    function statusOrderChange(data) {
        $("#" + data.id).html();
        $.ajax({
            method: "POST",
            url: "{{route('manager.editOrder')}}",
            data: {"_token": "{{ csrf_token() }}", data}
        }).done(function (msg) {

            showalert("Status was update");

        });

    }

    function showalert(message) {

        $('#alert_placeholder').append(
            '<div id="alter-success" class="alert alert-success alert-dismissible fade show mt-2" role="alert">'
            + message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button></div>'
        );
        window.scrollTo(0, 0);
    }
</script>

