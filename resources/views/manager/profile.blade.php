@extends('adminlte::page')

@section('content')
    <div class="d-flex justify-content-center">
        <div class="card mb-3" style="width: 540px;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="" class="img-fluid rounded-start" alt="Your Photo">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">{{$user->name}}</h5>
                        <p class="card-text">{{$user->email}}</p>
                        <p class="card-text"><small class="text-muted">Date of
                                registration: {{$user->created_at}}</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
