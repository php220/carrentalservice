@extends('layouts.app')

@section('content')
    @include('partials.partials')
    <div class="d-flex justify-content-center">
        <div class="card mb-3" style="width: 540px;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="" class="img-fluid rounded-start" alt="Your Photo">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">{{$user->name}}</h5>
                        <p class="card-text">{{$user->email}}</p>
                        <p class="card-text"><small class="text-muted">Date of
                                registration: {{$user->created_at}}</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{route('profile.update', ['user' => $user->id])}}">
        @method('PUT')
        @csrf
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Profile</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="email">Your email address</label>
                            <input type="email" name="email" id="email"
                                   class="form-control"
                                   value="{{old('email') ?? $user->email}}" disabled>
                        </div>
                    </div>

                    <div class="card-body mt-2">
                        <div class="form-group">
                            <label for="name">Enter your new name</label>
                            <input type="text" name="name" id="name"
                                   class="form-control @error('name') is-invalid @enderror"
                                   value="{{old('name') ?? $user->name}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                    </div>
                    <input type="submit" class="btn btn-success mt-2" value="Edit">
                </div>
            </div>
        </div>
    </form>

@endsection
