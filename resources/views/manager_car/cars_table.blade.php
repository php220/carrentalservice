<div class="row">
    <div class="col-12 mt-3">
        <div class="card">
            @include('partials.partials')
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="cars" class="table table-bordered table-hover dataTable dtr-inline"
                                   aria-describedby="example2_info">
                                <thead>
                                <tr>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1"
                                        aria-label="Rendering engine: activate to sort column ascending"
                                        style="width: 10%">Id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Browser: activate to sort column ascending"
                                        style="width: 20%">Brand and Models
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                        style="width: 10%">
                                        Status
                                    </th>
                                    <th class="sorting sorting_desc" tabindex="0" aria-controls="example2"
                                        rowspan="1" colspan="1"
                                        aria-label="Engine version: activate to sort column ascending"
                                        aria-sort="descending" style="width: 15%">Price per day
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="CSS grade: activate to sort column ascending"
                                        style="width: 20%">Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cars as $car)
                                    <tr>
                                        <td class="dtr-control" tabindex="0">{{$car->id}}</td>
                                        <td>{{$car->brand . ' ' .$car->model}}</td>
                                        <td>
                                            <div class="report-change-car-status">
                                                <select name="is_active" id="is_active_{{$car->id}}"
                                                        class="form-control w-100">

                                                    <option @if($car->is_active) selected
                                                            @endif value="1">Active
                                                    </option>

                                                    <option @if(!$car->is_active) selected
                                                            @endif value="0">Locked
                                                    </option>

                                                </select>
                                            </div>
                                        </td>
                                        <td class="sorting_1">$@convert($car->price_per_day)</td>
                                        <td>
                                            <div>
                                                <a class="btn btn-primary btn-sm" href="{{route('car.show', ["car" => $car->id])}}">
                                                    <i class="fas fa-folder">
                                                    </i>
                                                    View
                                                </a>
                                                <a class="btn btn-info btn-sm" href="{{route('car.edit', ["car" => $car->id])}}">
                                                    <i class="fas fa-pencil-alt">
                                                    </i>
                                                    Edit Car
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">Id</th>
                                    <th rowspan="1" colspan="1">Name</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Price per day</th>
                                    <th rowspan="1" colspan="1">Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<script>
    $(function () {
        $('#cars').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $(".report-change-car-status").on('change', function (event) {
        const data =
            {
                is_active: event.target.value,
                id: event.target.id,
            }
        statusCarChange(data)
    });

    function statusCarChange(data) {
        $("#" + data.id).html();
        $.ajax({
            method: "POST",
            url: "{{route('manager.editCar')}}",
            data: {"_token": "{{ csrf_token() }}", data}
        }).done(function (msg) {

            showalert("Status was update");

        });
    }

    function showalert(message) {

        $('#alert_placeholder').append(
            '<div id="alter-success" class="alert alert-success alert-dismissible fade show mt-2" role="alert">'
            + message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button></div>'
        );
        window.scrollTo(0, 0);
    }
</script>
