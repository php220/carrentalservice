@extends('adminlte::page')

@section('content')
    <form enctype="multipart/form-data" method="POST" action="{{ route('car.update', ['car' => $car->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            @include('partials.partials')
            <div class="col-md-12 mt-3">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit car</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="brand">Brand</label>
                                    <input type="text" id="brand" name="brand"
                                           class="form-control @error('brand') is-invalid @enderror"
                                           value="{{old('brand') ?? $car->brand }}">
                                    @error('brand')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="model">Model</label>
                                    <input id="model" name="model"
                                           class="form-control @error('model') is-invalid @enderror"
                                           value="{{old('model') ?? $car->model }}">
                                    @error('model')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <input id="type" name="type"
                                           class="form-control @error('type') is-invalid @enderror"
                                           value="{{old('type') ?? $car->type }}">
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="box_type">Box type</label>
                                    <input id="box_type" name="box_type"
                                           class="form-control @error('box_type') is-invalid @enderror"
                                           value="{{old('box_type') ?? $car->box_type }}">
                                    @error('box_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fuel_type">Fuel type</label>
                                    <input id="fuel_type" name="fuel_type"
                                           class="form-control @error('fuel_type') is-invalid @enderror"
                                           value="{{old('fuel_type') ?? $car->fuel_type }}">
                                    @error('fuel_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="seating">Seating</label>
                            <input id="seating" name="seating" class="form-control
                            @error('seating') is-invalid @enderror"
                                   value="{{old('seating') ?? $car->seating }}">
                            @error('seating')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price_per_day">Price per day</label>
                            <input id="price_per_day" name="price_per_day" class="form-control
                            @error('price_per_day') is-invalid @enderror"
                                   value="{{old('price_per_day') ?? $car->price_per_day }}">
                            @error('price_per_day')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>

                            <select name="is_active" id="status"
                                    class="form-control custom-select @error('is_active') is-invalid @enderror">
                                <option @if( old('is_active') && $car->is_active === old('is_active'))
                                            selected
                                        @elseif(empty(old('is_active')) && $car->is_active)
                                            selected
                                        @endif value="1">Active</option>

                                <option @if( old('is_active') && $car->is_active === old('is_active'))
                                            selected
                                        @elseif(empty(old('is_active')) && !$car->is_active)
                                            selected
                                        @endif value="0">Locked</option>

                            </select>
                            @error('is_active')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        @if($car->img != null && file_exists(storage_path('/app/public/images/'.$car->img)))
                            <div>
                                <img class="profile-user-img img-fluid" src="{{url('storage/images/'.$car->img)}}">
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="image">File</label>
                            <input name="image" id="image" type="file"
                                   class="form-control @error('image') is-invalid @enderror" placeholder="">
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Save Changes" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop


