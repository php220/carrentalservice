@extends('adminlte::page')

@section('content')
    <div id="alert_placeholder"></div>

    @include('manager_car.cars_table')
@stop

@section('js')
    <script>
        $(function () {
            $('#cars').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>

    <script>
        if (document.addEventListener) {
            document.addEventListener("change", function (event) {
                const data = {
                    is_active: event.target.value,
                    id: event.target.id,
                }
                statusCarChange(data)
            });

            function statusCarChange(data) {
                $("#" + data.id).html();
                $.ajax({
                    method: "POST",
                    url: "{{route('manager.editCar')}}",
                    data: {"_token": "{{ csrf_token() }}", data}
                }).done(function (msg) {

                    showalert("Status was update")

                });
            }
        }

        function showalert(message) {

            $('#alert_placeholder').append(
                '<div id="alter-success" class="alert alert-success alert-dismissible fade show mt-2" role="alert">'
                +message+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>' +
                '</button></div>'
            );
            window.scrollTo(0, 0);
        }
    </script>
@stop

