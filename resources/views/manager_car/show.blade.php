@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.partials')
        <div class="col-md-12">
            <div class="card">
                <div class="card-body box-profile">
                    @if($car->img != null && file_exists(storage_path('/app/public/images/'.$car->img)))
                    <div class="text-center">
                        <img class="profile-user-img img-fluid" src="{{url('storage/images/'.$car->img)}}">
                    </div>
                    @else
                        <div class="text-center">
                            <img class="profile-user-img img-fluid" src="" alt="Car picture">
                        </div>
                    @endif
                    <h3 class="profile-username text-center">#{{ $car->id }} {{ $car->brand }}</h3>

                    <p class="text-muted text-center">{{ $car->model }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Type</b> <a class="float-right">{{$car->type}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Box type</b> <a class="float-right">{{$car->box_type}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Fuel type</b> <a class="float-right">{{$car->fuel_type}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Seating</b> <a class="float-right">{{$car->seating}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Price per days</b> <a class="float-right">$@convert($car->price_per_day)</a>
                        </li>
                        <li class="list-group-item">
                            <b>Creation Date</b> <a class="float-right">{{$car->created_at}}</a>
                        </li>
                    </ul>

                    <a href="{{route('car.edit', ["car" => $car->id])}}" class="btn btn-primary btn-block">
                        <b>Edit</b>
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@stop


