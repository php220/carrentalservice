<form class="mb-3" action="{{ route('order.search') }}">
    @csrf
    <div class="row">
        <div class="col-md-11">
            <input class="form-control mr-sm-2" name="search" type="text" value="" placeholder="Search">
        </div>
        <div class="col-md-1">
            <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
        </div>
    </div>
</form>
