# CarRentalService

1. cp .env.example .env
2. composer install
3. npm install
4. npm run build
5. php artisan key:generate
6. chmod 755 -R storage/
7. php artisan storage:link

8. composer require algolia/algoliasearch-client-php
	8.1 registration on algonia 
	8.2 add in .env file:

	SCOUT_QUEUE=true

	ALGOLIA_APP_ID=your application id
	ALGOLIA_SECRET=admin api key

9. php artisan migrate
10. php artisan db:seed
