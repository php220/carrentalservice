<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class OrderRepository extends BaseRepository
{
    public int $paginateCount = 9;
    /**
     * @param \App\Models\Order $model
     */
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * Method for returning collection of new orders who added in a certain period of time with pagination
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function newOrdersDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->paginate($this->paginateCount);
    }

        /**
     * Method for returning collection of new orders who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newOrdersDuringThePeriod(string $startDate, string $endDate): EloquentCollection
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get();
    }

    /**
     * Method for returning the number of new orders for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function countNewOrdersDuringThePeriod(string $startDate, string $endDate): int
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get()->count();
    }

    /**
     * Method for returning approve orders
     *
     * @return \Illuminate\Support\Collection
     */
    public function getActiveOrders(): Collection
    {
        return $this->model->where('status', 'approve')->get();
    }

    /**
     * Method for sorting the column data of an orders table for the passed keyword
     *
     * @param string $column
     * @param string $keyword
     * @param int $userId
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function orderByOrdersByOrderData(string $column, string $keyword): LengthAwarePaginator
    {
        return $this->model->orderBy($column, $keyword)->paginate($this->paginateCount);
    }
}
