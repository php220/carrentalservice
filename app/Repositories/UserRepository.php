<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class UserRepository extends BaseRepository
{
    public int $paginateCount = 9;
    /**
     * @param  \App\Models\User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Method for counting active users
     *
     * @return int
     */
    public function activeUsersCount(): int
    {
        return $this->model->where("type", "user")->where("is_active", 1)->count();
    }

    /**
     * Method for show all users with type 'user'
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function showUsers(): LengthAwarePaginator
    {
        return $this->model->whereNot('type', 'manager')->paginate($this->paginateCount);
    }

    /**
     * Method for returning collection of new users who registered in a certain period of time with pagination
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function newUsersDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereNot('type', 'manager')->paginate($this->paginateCount);
    }

    /**
     * Method for returning collection of new users who registered in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newUsersDuringThePeriod(string $startDate, string $endDate): Collection
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereNot('type', 'manager')->get();
    }

    /**
     * Method for returning the number of new users for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function countNewUsersDuringThePeriod(string $startDate, string $endDate): int
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->whereNot('type', 'manager')->get()->count();
    }

    /**
     * Method for sorting the column data of an user table for the passed keyword
     *
     * @param string $column
     * @param string $keyword
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function orderByUsersByOrdersData(string $column, string $keyword): LengthAwarePaginator
    {
        return $this->model->orderBy($column, $keyword)->paginate($this->paginateCount);
    }
}
