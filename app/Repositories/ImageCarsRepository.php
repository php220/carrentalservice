<?php

namespace App\Repositories;

use App\Models\ImageCars;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

class ImageCarsRepository extends BaseRepository
{
    /**
     * @param \App\Models\ImageCars $model
     */
    public function __construct(ImageCars $model)
    {
        $this->model = $model;
    }

    /**
     * Methods for getting car images
     *
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCarImages(int $id): Collection
    {
        return $this->model->select('image')->where('car_id', $id)->get();
    }

    /**
     * Method for deleting car images
     *
     * @param integer $id
     * @param string $image
     * @return void
     */
    public function deleteCarImages(int $id, string $image)
    {
        $this->model->where('car_id', $id)->where('image', $image)->delete();
    }
}
