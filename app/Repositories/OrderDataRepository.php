<?php

namespace App\Repositories;

use App\Models\OrderData;
use Illuminate\Support\Collection;
use App\Repositories\BaseRepository;

class OrderDataRepository extends BaseRepository
{
    /**
     * @param \App\Models\OrderData $model
     */
    public function __construct(OrderData $model)
    {
        $this->model = $model;
    }

    /**
     * Method for returning collection of new orders data who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Support\Collection
     */
    public function newOrdersDataDuringThePeriod(string $startDate, string $endDate): Collection|string
    {
        return $this->model->where('created_at', '>=', $startDate)
        ->where('created_at', '<=', $endDate)
        ->get();
    }

    /**
     * Method for getting sub orders by order id
     *
     * @param integer $orderId
     * @return \Illuminate\Support\Collection
     */
    public function getSubOrdersByOrderId(int $orderId): Collection
    {
        return $this->model->where('order_id', $orderId)->get();
    }
}
