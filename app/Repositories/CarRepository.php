<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class CarRepository extends BaseRepository
{
    public int $paginateCount = 9;
    /**
     * @param \App\Models\Car
     */
    public function __construct(Car $model)
    {
        $this->model = $model;
    }

    /**
     * Method for returning collection of new cars who added in a certain period of time with pagination
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function newCarsDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->paginate($this->paginateCount);
    }

    /**
     * Method for returning collection of new cars who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCarsDuringThePeriod(string $startDate, string $endDate): Collection
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get();
    }

    /**
     * Method for returning the number of new cars for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function countNewCarsDuringThePeriod(string $startDate, string $endDate): int
    {
        return $this->model->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get()->count();
    }

    /**
     * Method for sorting the column data of an cars table for the passed keyword
     *
     * @param string $column
     * @param string $keyword
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function orderByCarsData(string $column, string $keyword): LengthAwarePaginator
    {
        return $this->model->orderBy($column, $keyword)->paginate($this->paginateCount);
    }
}
