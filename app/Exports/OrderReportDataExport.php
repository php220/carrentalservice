<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderReportDataExport implements  FromView,ShouldAutoSize
{
    use Exportable;

    /**
     * @var \Illuminate\Support\Collection
     */
    private Collection $orderData;

    /**
     * @param \Illuminate\Support\Collection $orders
     */
    public function __construct(Collection $orderData)
    {
        $this->orderData = $orderData;
    }

    public function view(): View
    {
        return view('manager.report.orders_report_table', ['orders' => $this->orderData]);
    }
}
