<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CarReportDataExport implements FromView, ShouldAutoSize
{
    use Exportable;

    /**
     * @var \Illuminate\Support\Collection
     */
    private Collection $cars;

    /**
     * @param \Illuminate\Support\Collection $orders
     */
    public function __construct(Collection $cars)
    {
        $this->cars = $cars;
    }

    public function view(): View
    {
        return view('manager.report.cars_report_table', ['cars' => $this->cars]);
    }
}
