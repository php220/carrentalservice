<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UserReportDataExport implements  FromView,ShouldAutoSize
{
    use Exportable;

    /**
     * @var \Illuminate\Support\Collection
     */
    private Collection $users;

    /**
     * @param \Illuminate\Support\Collection $users
     */
    public function __construct(Collection $users)
    {
        $this->users = $users;
    }

    public function view(): View
    {
        return view('manager.report.users_report_table', ['users' => $this->users]);
    }
}
