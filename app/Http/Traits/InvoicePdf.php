<?php

namespace App\Http\Traits;

use Barryvdh\DomPDF\Facade\Pdf;

trait InvoicePdf
{
    /**
     * @param string $reportTemplate
     * @param string $fileName
     * @param array $data
     * @return mixed
     */
    public function pdf(string $reportTemplate, string $fileName, array $data)
    {
        return Pdf::loadView($reportTemplate, $data)->download($fileName . ".pdf");
    }
}
