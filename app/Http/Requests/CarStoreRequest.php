<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'numberOfDays' => 'required|array',
            'numberOfDays.*' => 'required|integer|between:1,10'
        ];
    }

    public function messages()
    {
        return [
            'numberOfDays.*.integer' => 'Number of days must be integer',
            'numberOfDays.*.required' => 'You must select the number of days!',
            'numberOfDays.*.between' => 'The number of days selected must be from 1 to 10!',
        ];
    }

}
