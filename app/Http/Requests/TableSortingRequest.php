<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TableSortingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'table' => 'required|string|in:orders,users,cars',
            'column' => 'required|string|in:id,name,email,created_at,brand,price_per_day,user_id',
            'keyword' => 'required|string|in:desc,asc',

        ];
    }
}
