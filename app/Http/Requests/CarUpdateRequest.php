<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CarUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'brand' =>
                [
                    'required',
                    'max:50',
                    'min:3',
                    Rule::unique('cars')
                        ->ignore($this->car)
                        ->where('model', $this->model)
                        ->where('type', $this->type)
                ],
            'model' => 'required|max:50|min:3',
            'type' => 'required|max:50|min:3',
            'box_type' => 'required|max:50|min:3',
            'fuel_type' => 'required|max:50|min:3',
            'seating' => 'required|numeric|integer|between:1,7',
            'price_per_day' => 'required|numeric|between:1,1000',
            'is_active' => 'required|in:0,1',
            'image' => 'required|array'
        ];
    }
}
