<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Order;
use App\Mail\NewUserOrder;
use App\Services\CarService;
use Illuminate\Http\Request;
use App\Mail\OrderInstruction;
use App\Services\OrderService;
use Illuminate\Support\Carbon;
use App\Http\Traits\InvoicePdf;
use App\Services\ImageCarsService;
use App\Services\OrderDataService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderStatusChangeManager;
use App\Http\Requests\OrderConfirmRequest;

class OrderController extends Controller
{
    use InvoicePdf;

    /**
     * The class properties
     *
     * @var \App\Services\OrderService
     * @var \App\Services\CarService
     */
    public OrderService $order;
    public CarService $car;
    public OrderDataService $orderData;
    public ImageCarsService $imageCars;

    /**
     * @param \App\Services\OrderService $order
     * @param \App\Services\CarService $car
     * @param \App\Services\OrderDataService $orderData
     * @param \App\Services\ImageCarsService $imageCars
     */
    public function __construct
    (
        OrderService $order,
        CarService $car,
        OrderDataService $orderData,
        ImageCarsService $imageCars
    ){
        $this->order = $order;
        $this->car = $car;
        $this->orderData = $orderData;
        $this->imageCars = $imageCars;
    }

    /**
     * Method for displaying user orders with pagination
     *
     * @param \App\Models\User $user
     * @return \Inertia\Inertia
     */
    public function index(User $user)
    {
        return Inertia::render('Orders/Index', [
            "orders" => $user->orders()->orderBy('id', 'DESC')->paginate(6),
        ]);
    }

    /**
     * Method for displaying additionally information of users orders
     *
     * @param \App\Models\Order $order
     * @return \Inertia\Inertia
     */
    public function ordersAdditionallyInfo(Order $order)
    {
        return Inertia::render('Orders/AdditionallyInfo',
            [
                'ordersInfo' => $order->subOrder()->get(),
                'carsImage' => $this->imageCars->all(),
            ]
        );
    }

    /**
     * Method for finding orders
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Inertia\Inertia
     *
     */
    public function search(Request $request)
    {
        if ($request->query('query') === null) {
            return Inertia::render('Orders/Index',
                [
                    'orders' => $this->order->getAllWithPagination(),
                ]
            );
        }

        return Inertia::render('Orders/Index',
            [
                'orders' => Order::search($request->query('query'))->paginate(6),
            ]
        );
    }

    /**
     * Method for creating an order and adding a car to that order, as well as for sending emails to the mail
     *
     * @param \Illuminate\Http\Requests\OrderConfirmRequest $request
     * @param \App\Models\User $user
     * @return \Inertia\Inertia
     */
    public function addNew(OrderConfirmRequest $request, User $user)
    {
        $order = $this->order->create([
            'user_id' => $user->id
        ]);

        foreach ($request->except('_token') as $value) {
            $value = explode(',', $value);

            $car = $this->car->findById($value[0]);

            $car->is_active = 0;
            $car->save();

            $this->orderData->create([
                'order_id' => $order->id,
                'car_id' => $car->id,
                'sum' => $car->price_per_day * $value[1],
                'number_of_days' => $value[1],
                'end_of_lease' => Carbon::now()->addDays($value[1])
            ]);
        }

        Mail::to($user->email)->send(new OrderInstruction($user));
        Mail::to(config('app.managerEmail'))->send(new NewUserOrder($order, $user));

        return redirect()->route('order', ['user' => $user->id]);
    }

    /**
     * Method for canceling new orders in user orders table
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Order $order)
    {
        if($order->status->value === 'new') {

            foreach ($order->subOrder()->get() as $subOrder) {
                $subOrder->is_active = 0;
                $subOrder->save();

                $car = $this->car->findById($subOrder->car_id);

                $car->is_active = 1;
                $car->save();
            }

        }

        $order->update(['status' => 'cancel']);

        Mail::to(config('app.managerEmail'))->send(new OrderStatusChangeManager($order, Auth::user()));

        return redirect()->route('order',['user' => Auth::id()])->with(['deleteSuccess' => 'ok']);
    }

    /**
     * Method for getting all information about the order in PDF format
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function invoicePdf(Order $order)
    {
        $data = [
            "order" => $order,
            "totalPrice" => array_sum(array_column($order->subOrder()->get()->toArray(), 'sum')),
            "user" => Auth::user()
        ];

        return $this->pdf(
            'orders.invoicepdf',
            "OrderData" . $order->id,
            $data
        );
    }

}
