<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Inertia\Inertia;
use App\Services\CarService;
use App\Services\ImageCarsService;
use App\Http\Requests\CarUpdateRequest;
use App\Http\Requests\CreateNewCarRequest;
use Carbon\Carbon;

class ManagerCarController extends Controller
{
    /**
     * The class properties
     *
     * @var \App\Services\CarService
     * @var \App\Services\ImageCarsService
     */
    public CarService $car;
    public ImageCarsService $imageCars;

    /**
     * @param \App\Services\CarService $car
     * @param \App\Services\ImageCarsService $imageCars
     */
    public function __construct(CarService $car, ImageCarsService $imageCars)
    {
        $this->car = $car;
        $this->imageCars = $imageCars;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Inertia
     */
    public function index()
    {
        return Inertia::render('CRUDCars/Index',
            [
                'cars' => $this->car->getAllWithPagination(),
                'images' => $this->imageCars->all()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Inertia
     */
    public function create()
    {
        return Inertia::render('CRUDCars/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateNewCarRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateNewCarRequest $request)
    {
        $car = $this->car->create($request->except('image', 'is_active'));

        $car->is_active = $request->is_active;
        $car->save();

        if ($request->image) {

            foreach ($request->file('image') as $key => $image) {
                $carImage = $car->brand . '_' . $car->model . "_" . hash('sha256', Carbon::now()) . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/images/' . $car->id, $carImage);

                $this->imageCars->create([
                    'car_id' => $car->id,
                    'image' => $carImage,
                    'default' => ($key === 0) ? 1 : 0
                ]);
            }

        }

        return redirect()->route('car.create')->with(['updateSuccess' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Car $car
     *
     * @return \Inertia\Inertia
     */
    public function show(Car $car)
    {
        return Inertia::render('CRUDCars/Show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Car $car
     *
     * @return \Inertia\Inertia
     */
    public function edit(Car $car)
    {
        return Inertia::render('CRUDCars/Edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\CarUpdateRequest $request
     * @param \App\Models\Car $car
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CarUpdateRequest $request, Car $car)
    {
        $car->update($request->except(['_method', '_token', 'image', 'is_active']));

        $car->is_active = $request->is_active;
        $car->save();

        $arrayImagesCar = $this->imageCars->getCarImages($car->id)->toArray();

        foreach ($request->image as $value) {
            foreach ($arrayImagesCar as $image) {
                if (gettype($value) === 'array' && $value['name'] === $image['image']) {
                    $index = array_search($image, $arrayImagesCar);
                    unset($arrayImagesCar[$index]);
                }
            }
        }

        $this->imageCars->deleteCarImages($car->id, $arrayImagesCar);

        foreach($arrayImagesCar as $image) {
            array_map('unlink', glob(storage_path('app/public/images/' . $car->id . '/' . $image['image'])));
        }

        array_filter($request->image, function($image) use ($car){
            if (gettype($image) === 'object') {
                $carImage = $car->brand . '_' . $car->model . "_"
                . hash('sha256', $image->getClientOriginalName())
                . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/images/' . $car->id, $carImage);

                $this->imageCars->create(
                    [
                        'car_id' => $car->id,
                        'image' => $carImage,
                        // 'default' => ($key === 0) ? 1 : 0,
                        'size' => $image->getSize(),
                    ]
                );
            }
        });

        return redirect()->route('car.index')->with(['updateSuccess' => "success"]);
    }
}
