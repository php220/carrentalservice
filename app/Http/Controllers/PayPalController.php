<?php

namespace App\Http\Controllers;
use Inertia\Inertia;
use Omnipay\Omnipay;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Mail\PaymentWasSuccessful;
use Illuminate\Support\Facades\Mail;

class PayPalController extends Controller
{
    private $gateway;
    public OrderService $order;

    public function __construct(OrderService $order) {
        $this->order = $order;
        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->initialize([
            'clientId' => env('PAYPAL_CLIENT_ID'),
            'secret' => env('PAYPAL_APP_SECRET'),
            'testMode' => true,
        ]);
    }

    public function pay(Request $request)
    {
        $response = $this->gateway->purchase([
            'amount' => $request->all()['totalAmount'],
            'description' => $request->all()['orderId'],
            'currency' => env('PAYPAL_CURRENCY'),
            'returnUrl' => url('/success'),
            'cancelUrl' => url('/error'),
        ])->send();

        if ($response->isRedirect()) {
            $response->redirect();
        }
    }

    public function success(Request $request)
    {
        if ($request->input('paymentId') && $request->input('PayerID')) {
            $transaction = $this->gateway->completePurchase([
                'payer_id' => $request->input('PayerID'),
                'transactionReference' => $request->input('paymentId')
            ]);

            $response = $transaction->send();

            if ($response->isSuccessful()) {

                $arr = $response->getData();

                $order = $this->order->findById($arr['transactions'][0]["description"]);

                $order->status = 'paid';
                $order->payment_id = $arr['id'];
                $order->save();

                Mail::to(config('app.managerEmail'))->send(new PaymentWasSuccessful($order));

                return Inertia::render('My/Thanks');
            }
            else{
                return $response->getMessage();
            }
        }
        else{
            return 'Payment declined!!';
        }
    }

    public function error()
    {
        return 'User declined the payment!';
    }
}
