<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Car;
use App\Models\User;
use Inertia\Inertia;
use App\Models\Order;
use App\Models\OrderData;
use App\Services\CarService;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Enums\OrderStatusEnum;
use App\Services\OrderService;
use App\Mail\OrderStatusChange;
use App\Exceptions\FileNotFound;
use App\Services\OrderDataService;
use App\Exports\CarReportDataExport;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserReportDataExport;
use App\Services\ImageCarsService;
use App\Exports\OrderReportDataExport;
use App\Exceptions\DateReportException;
use App\Http\Requests\ReportDateRequest;
use App\Http\Requests\TableSortingRequest;
use App\Http\Requests\CarUpdateStatusRequest;
use App\Http\Requests\UserUpdateStatusRequest;
use App\Http\Requests\OrderUpdateStatusRequest;
use App\Http\Requests\AdditionDataReportRequest;
use App\Http\Requests\CarUpdateStatusInOrderDataRequest;

class ManagerController extends Controller
{
    /**
     * The class properties
     *
     * @var \App\Services\UserService
     * @var \App\Services\CarService
     * @var \App\Services\OrderService
     * @var \App\Services\OrderDataService
     * @var \App\Services\ImageCarsService
     */
    public UserService $user;
    public CarService $car;
    public OrderService $order;
    public OrderDataService $orderData;
    public ImageCarsService $imageCars;

    /**
     * @param \App\Services\UserService $user
     * @param \App\Services\CarService $car
     * @param \App\Services\OrderService $order
     * @param \App\Services\ImageCarsService $imageCars
     */
    public function __construct
    (
        UserService $user,
        CarService $car,
        OrderService $order,
        OrderDataService $orderData,
        ImageCarsService $imageCars
    ) {
        $this->user = $user;
        $this->car = $car;
        $this->order = $order;
        $this->orderData = $orderData;
        $this->imageCars = $imageCars;
    }

    /**
     * Method for displaying home manager page
     *
     * @return \Inertia\Inertia
     */
    public function index()
    {
        return Inertia::render('Manager/Index',
            [
                'usersCount' => $this->user->activeUsersCount(),
                'carsCount' => $this->car->count(),
                'ordersCount' => $this->order->count(),
            ]
        );
    }

    /**
     * Method for sorting cars
     *
     * @param \App\Http\Requests\TableSortingRequest $request
     * @return \Illuminate\Contracts\View\View
     */
    public function sorting(TableSortingRequest $request)
    {
        $requestData = $request->query();

        if ($requestData['table'] === 'users') {
            return Inertia::render('Manager/Users',
                [
                    'users' => $this->user->orderByUsersByOrdersData($requestData['column'], $requestData['keyword'])->withQueryString(),
                ]
            );
        }

        if ($requestData['table'] === 'cars') {
            return Inertia::render('CRUDCars/Index',
                [
                    'cars' => $this->car->orderByCarsData($requestData['column'], $requestData['keyword'])->withQueryString(),
                    'images' => $this->imageCars->all()
                ]
            );
        }

        if ($requestData['table'] === 'orders') {
            return Inertia::render('Manager/Orders',
                [
                    'orders' => $this->order->orderByOrdersByOrderData($requestData['column'], $requestData['keyword'])->withQueryString(),
                    'statuses' => OrderStatusEnum::values(),
                ]
            );
        }
    }

    /**
     * Method for finding cars
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Inertia\Inertia
     *
     */
    public function searchCar(Request $request)
    {
        if ($request->query('query') === null) {
            return Inertia::render('CRUDCars/Index',
                [
                    'cars' => $this->order->getAllWithPagination()->withQueryString(),
                ]
            );
        }

        return Inertia::render('CRUDCars/Index',
            [
                'cars' => Car::search($request->query('query'))->paginate(9),
            ]
        );
    }

    /**
     * Method for finding users
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Inertia\Inertia
     *
     */
    public function searchUser(Request $request)
    {
        if ($request->query('query') === null) {
            return Inertia::render('Manager/Users',
                [
                    'users' => $this->order->getAllWithPagination()->withQueryString(),
                ]
            );
        }

        return Inertia::render('Manager/Users',
            [
                'users' => User::search($request->query('query'))->paginate(9),
            ]
        );
    }

    /**
     * Method for finding orders
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Inertia\Inertia
     *
     */
    public function searchOrder(Request $request)
    {
        if ($request->query('query') === null) {
            return Inertia::render('Manager/Orders',
                [
                    'orders' => $this->order->getAllWithPagination(),
                    'statuses' => OrderStatusEnum::values(),
                ]
            );
        }

        return Inertia::render('Manager/Orders',
            [
                'orders' => Order::search($request->query('query'))->paginate(9),
                'statuses' => OrderStatusEnum::values(),
            ]
        );
    }

    /**
     * Method for displaying all users table
     *
     * @return \Inertia\Inertia
     */
    public function displayAllUsers()
    {
        return Inertia::render('Manager/Users',
            [
                "users" => $this->user->showUsers()
            ]
        );
    }

    /**
     * Method for edit status user
     *
     * @param \App\Http\Requests\UserUpdateStatusRequest $request
     * @return Illuminate\Http\Response
     */
    public function editStatusUser(UserUpdateStatusRequest $request)
    {
        $requestData = $request->validated();

        $user = $this->user->findById($requestData["id"]);

        $user->is_active = $requestData['status'];
        $user->save();

        return response()->json('Success');
    }

    /**
     * Method for displaying orders table
     *
     * @return \Inertia\Inertia
     */
    public function displayAllOrders()
    {
        return Inertia::render('Manager/Orders',
            [
                'orders' => $this->order->getAllWithPagination(),
                'statuses' => OrderStatusEnum::values(),
            ]
        );
    }

    /**
     * Method for edit status order
     *
     * @param \App\Http\Requests\OrderUpdateStatusRequest $request
     * @return Illuminate\Http\Response
     */

    public function editStatusOrder(OrderUpdateStatusRequest $request)
    {
        $requestData = $request->validated();

        $order = $this->order->findById($requestData["id"]);

        if ($requestData['status'] === 'cancel') {

            foreach ($order->subOrder()->get() as $subOrder) {
                $subOrder->is_active = 0;
                $subOrder->save();

                $subOrder->car()->first()->update(["is_active" => 1]);
            }

        }

        $order->status = $requestData['status'];
        $order->save();

        Mail::to($order->user()->first()->email)->send(new OrderStatusChange(
            $order,
            $order->user()->first()
        ));

        return response()->json('Success');
    }

    /**
     * Method for edit car status immediately on the page with the table
     *
     * @param \App\Http\Requests\CarUpdateStatusRequest $request
     * @return Illuminate\Http\Response
     */
    public function editStatusCar(CarUpdateStatusRequest $request)
    {
        $requestData = $request->validated();

        $car = $this->car->findById($requestData['id']);

        $car->is_active = $requestData['status'];
        $car->save();

        return response()->json('Success');
    }

    /**
     * Method for update car status in order data table
     *
     * @param \App\Http\Requests\CarUpdateStatusInOrderDataRequest $request
     * @return mixed
     */
    public function updateCarStatusInOrderData(CarUpdateStatusInOrderDataRequest $request)
    {
        $carInOrderData = OrderData::where('order_id', $request->all()['orderId'])->where('car_id', $request->all()['id'])->first();
        $carInOrderData->is_active = $request->all()['status'];
        $carInOrderData->save();
    }

    /**
     * Method for displaying report manager page
     *
     * @return \Inertia\Inertia
     */
    public function report()
    {
        return Inertia::render('Manager/Report');
    }

    /**
     * Method for displaying manager table with report data
     *
     * @param \App\Http\Requests\ReportDateRequest $request
     * @return \Inertia\Inertia
     */
    public function reportData(ReportDateRequest $request)
    {
        $date = explode(',', $request->validated()['date']);
        try {
            if (
                $date[0] < $date[1]
                && $date[0] <= Carbon::now()
                && $date[1] <= Carbon::now()
               ) {
                    return Inertia::render('Manager/Report',
                        [
                            'newUsers' => $this->user->countNewUsersDuringThePeriod(
                                $date[0],
                                $date[1]
                            ),
                            'newCars' => $this->car->countNewCarsDuringThePeriod(
                                $date[0],
                                $date[1]
                            ),
                            'newOrders' => $this->order->countNewOrdersDuringThePeriod(
                                $date[0],
                                $date[1]
                            ),
                            'profit' => $this->order->newOrdersProfit(
                                $date[0],
                                $date[1]
                            ),
                            'startDate' => $date[0],
                            'endDate' => $date[1]
                        ]
                    );
                }

            throw new DateReportException();

        } catch (DateReportException $e) {
            report($e);

            return Inertia::render('Manager/Report', ['error' => $e->getMessage()]);
        }
    }

    /**
     * Method for displaying additional data on the report page
     *
     * @param \App\Http\Requests\AdditionDataReportRequest $request
     * @return \Illuminate\Contracts\View\View
     */
    public function reportAdditionData(AdditionDataReportRequest $request)
    {
        $data = $request->validated();

        switch ($data['table']) {

            case "users":
                return Inertia::render(
                    "Manager/Users",
                    [
                        'users' => $this->user->newUsersDuringThePeriodWithPagination(
                            $data["start"],
                            $data["end"]
                        )->withQueryString(),
                    ]
                );

            case "cars":
                return Inertia::render(
                    "CRUDCars/Index",
                    [
                        'cars' => $this->car->newCarsDuringThePeriodWithPagination(
                            $data["start"],
                            $data["end"]
                        )->withQueryString()
                    ]
                );

            case "orders":
                return redirect()->route('manager.orders',
                    [
                        'orders' => $this->order->newOrdersDuringThePeriodWithPagination(
                            $data["start"],
                            $data["end"]
                        )->withQueryString()
                    ]
                );

            default:
                return Inertia::render("Manager/Report");
        }
    }

    /**
     * Method of downloading a report of data in excel format separately for each table
     *
     * @param \App\Http\Requests\ReportExcelRequest $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse| \App\Exceptions\FileNotFound
     */
    //ReportExcelRequest
    public function reportExportExcel(Request $request)
    {
        $data = $request->all();

        try {
            switch ($data['table']) {

                case "users":
                    return Excel::download(new UserReportDataExport(
                        $this->user->newUsersDuringThePeriod(
                            $data["start"],
                            $data["end"]
                        )
                    ), 'reportUsers.xlsx');

                case "cars":
                    return Excel::download(new CarReportDataExport(
                        $this->car->newCarsDuringThePeriod(
                            $data["start"],
                            $data["end"]
                        )
                    ), 'reportCars.xlsx');

                case "orderdata":
                    return Excel::download(new OrderReportDataExport(
                        $this->orderData->newOrdersDataDuringThePeriod(
                            $data["start"],
                            $data["end"]
                        )
                    ), 'reportOrdersData.xlsx');

                default:
                    throw new FileNotFound();
            }
        } catch (FileNotFound $e) {
            report($e);
        }
    }
}
