<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserDataRequest;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * The class properties
     *
     * @var \App\Services\UserService
     */
    public UserService $user;

    /**
     * @param \App\Services\UserService $user
     */
    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * Method for displaying profile user page
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Contracts\View\View
     */
    public function profile(User $user)
    {
        return view('users.profile', compact('user'));
    }

    /**
     * Method for editing user data on his profile page
     *
     * @param \App\Http\Requests\UpdateUserDataRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserDataRequest $request, User $user)
    {
        $user->update($request->except(['_method', '_token']));

        return redirect()->route('profile', ['user' => $user])->withSuccess(
            "Your data was updated!"
        );
    }
}
