<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Inertia\Inertia;
use App\Services\CarService;
use Illuminate\Http\Request;
use App\Services\ImageCarsService;

class CarController extends Controller
{
    /**
     * The class properties
     *
     * @var \App\Services\CarService
     * @var \App\Services\ImageCarsService
     */
    public CarService $car;
    public ImageCarsService $imageCars;

    /**
     * @param \App\Services\CarService $car
     */
    public function __construct(CarService $car, ImageCarsService $imageCars)
    {
        $this->car = $car;
        $this->imageCars = $imageCars;
    }

    /**
     * Method for displaying all cars with pagination
     *
     * @return \Inertia\Inertia
     */
    public function index()
    {
        return Inertia::render('Cars/DisplayAllCars',
            [
                'cars' => $this->car->getAllWithPagination(),
                'images' => $this->imageCars->all(),
            ]
        );
    }

    /**
     * Method for finding cars
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Inertia\Inertia
     *
     */
    public function search(Request $request)
    {
        if ($request->query('query') === null) {
            return Inertia::render('Cars/DisplayAllCars',
                [
                    'cars' => $this->car->getAllWithPagination(),
                    'images' => $this->imageCars->all(),
                ]
            );
        }

        return Inertia::render('Cars/DisplayAllCars',
            [
                'cars' => Car::search($request->query('query'))->paginate(9),
                'images' => $this->imageCars->all(),
            ]
        );
    }
}
