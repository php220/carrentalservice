<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class EditStatus extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct
    (
        string $message = "status is not update",
        int $code = Response::HTTP_UNPROCESSABLE_ENTITY
    ) {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * Get the exception's context information.
     *
     * @return array<string, mixed>
     */
    public function context(): array
    {
        return ['context' => 'The status has not been updated for some reason'];
    }
}
