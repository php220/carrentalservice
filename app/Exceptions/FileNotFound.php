<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class FileNotFound extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct
    (
        string $message = "This file not found",
        int $code = Response::HTTP_NOT_FOUND
    ) {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * Get the exception's context information.
     *
     * @return array<string, mixed>
     */
    public function context(): array
    {
        return ['context' => 'File not found'];
    }
}
