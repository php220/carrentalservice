<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class DateReportException extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct
    (
        string $message = "The date is not correct",
        int $code = Response::HTTP_UNPROCESSABLE_ENTITY
    ) {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * Get the exception's context information.
     *
     * @return array<string, mixed>
     */
    public function context(): array
    {
        return ['context' => 'The date is not correct'];
    }
}
