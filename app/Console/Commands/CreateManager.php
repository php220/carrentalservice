<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CreateManager extends Command
{
    /**
     * The class properties
     *
     * @var \App\Services\UserService
     */
    public UserService $user;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-manager';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create an account manager';

    /**
     * @param \App\Services\UserService $user
     */
    public function __construct(UserService $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $name = $this->ask('What is user\'s name?');
        $email = $this->ask('What is user\'s email?');
        $password = $this->ask('What is user\'s password?');

        $validator = Validator::make([
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ], [
            'name' => 'required|min:4|max:255',
            'email' => 'required|max:255|unique:users,email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $this->info('Manager account not created. See error messages below:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return 1;
        }

        $this->user->create(
            [
                'name' => $name,
                'email' => $email,
                'email_verified_at' => now(),
                'type' => 'manager',
                'password' => Hash::make($password),
            ]
        );

        $this->info('Manager account was successfully created');

        return 0;
    }
}
