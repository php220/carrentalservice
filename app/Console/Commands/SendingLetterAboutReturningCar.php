<?php

namespace App\Console\Commands;

use App\Mail\SendingLetterAboutReturningCarEmail;
use App\Services\OrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;

class SendingLetterAboutReturningCar extends Command
{
    /**
     * The class properties
     *
     * @var \App\Services\OrderService
     */
    public OrderService $order;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sending-letter-about-returning-car';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @param \App\Services\OrderService $order
     */
    public function __construct(OrderService $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $orders = $this->order->getActiveOrders();

        foreach ($orders as $order) {
            $date = Carbon::parse($order->end_of_lease);
            $result = $date->diffInHours(Carbon::now());

            if ($order->end_of_lease > Carbon::now() && $result <= 24) {
                $user = $order->user()->first();
                Mail::to($user->email)->send(new SendingLetterAboutReturningCarEmail($order, $user));
            }

        }
    }
}
