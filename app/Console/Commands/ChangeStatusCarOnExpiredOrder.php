<?php

namespace App\Console\Commands;

use App\Services\OrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ChangeStatusCarOnExpiredOrder extends Command
{
    /**
     * The class properties
     *
     * @var \App\Services\OrderService
     */
    public OrderService $order;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:change-status-car-on-expired-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changing the status of the car after the expiration of the lease';

    /**
     * @param \App\Services\OrderService $order
     */
    public function __construct(OrderService $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $orders = $this->order->getActiveOrders();

        foreach ($orders as $order) {

            if ($order->end_of_lease < Carbon::now()) {
                $order->update(['status' => 'completed']);
                $order->car()->first()->update(['status' => 'active']);
            }

        }
    }
}
