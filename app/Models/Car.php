<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Laravel\Scout\Searchable;

class Car extends Model
{
    use HasFactory, Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "cars";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ["brand", "model", "type", "box_type", "fuel_type", "seating", "price_per_day", "is_active"];

    /**
     * Attitude of the car to orders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderdata()
    {
        return $this->hasMany(OrderData::class, 'car_id', 'id');
    }

    // protected $with = ['orderdata'];

    /**
     * Accessor for the created_at column in car table
     *
     * @return Attribute
     */
    protected function CreatedAt(): Attribute
    {
        return Attribute::make(
            get: fn(string $value) => date("Y-m-d", strtotime($value)),
        );
    }
}
