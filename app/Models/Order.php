<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\OrderStatusEnum;
use Laravel\Scout\Searchable;

class Order extends Model
{
    use HasFactory;
    use Searchable;

    // {
    //     Searchable::search as parentSearch;
    // }
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "orders";

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $fillable = ["user_id", "status"];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'status' => OrderStatusEnum::class
    ];

    /**
     * Attitude of the order to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    /**
     * Attitude of the order to subOrder
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subOrder()
    {
        return $this->hasMany(OrderData::class,'order_id', 'id');
    }

    protected $with = ['subOrder', 'user'];
}
