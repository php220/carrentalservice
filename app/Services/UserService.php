<?php

namespace App\Services;

use App\Exceptions\DateReportException;
use App\Repositories\UserRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UserService extends BaseService
{
    /**
     * Property that stores columns for sorting users
     *
     * @var array|string[]
     */
    public array $columnForSorting = ['id', 'name', 'email', 'created_at'];

    /**
     * @param \App\Repositories\UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Method for counting active users
     *
     * @return int
     */
    public function activeUsersCount(): int
    {
        return $this->repo->activeUsersCount();
    }

    /**
     * Method for show all users with type 'user'
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function showUsers(): LengthAwarePaginator
    {
        return $this->repo->showUsers();
    }

    /**
     * Method for returning collection of new users who registered in a certain period of time with pagination
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function newUsersDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newUsersDuringThePeriodWithPagination($startDate, $endDate);
        } catch (DateReportException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Method for returning collection of new users who registered in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Collection|string
     */
    public function newUsersDuringThePeriod(string $startDate, string $endDate): Collection|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newUsersDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Method for returning the number of new users for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int|string
     */
    public function countNewUsersDuringThePeriod(string $startDate, string $endDate): int|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->countNewUsersDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Method for sorting data on users index page
     *
     * @param string $column
     * @param string $keyword
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function orderByUsersByOrdersData(string $column, string $keyword): LengthAwarePaginator|string
    {
        $keyword = strtolower($keyword);

        if (
            (empty($column) || empty($keyword))
            || !in_array($keyword, ["asc", "desc"])
            || !in_array($column, $this->columnForSorting)
        ) {

            return 'Sorting error';
        }

        return $this->repo->orderByUsersByOrdersData($column, $keyword);
    }

}
