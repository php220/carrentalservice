<?php

namespace App\Services;

use Carbon\Carbon;
use App\Services\BaseService;
use App\Services\OrderDataService;
use Illuminate\Support\Collection;
use App\Repositories\OrderRepository;
use App\Exceptions\DateReportException;
use Illuminate\Pagination\LengthAwarePaginator;

class OrderService extends BaseService
{
    public OrderDataService $subOrder;
    public array $columnForSorting = ['id', 'user_id', 'created_at'];
    /**
     * @param \App\Repositories\OrderRepository $repo
     * @param \App\Services\OrderDataService $subOrder
     */
    public function __construct(OrderRepository $repo, OrderDataService $subOrder)
    {
        $this->repo = $repo;
        $this->subOrder = $subOrder;
    }

    /**
     * Method for returning collection of new orders who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function newOrdersDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newOrdersDuringThePeriodWithPagination($startDate, $endDate);
        } catch (DateReportException $e) {

            return $e->getMessage();
        }
    }

    /**
     * Method for returning the number of new orders for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int|string
     */
    public function countNewOrdersDuringThePeriod(string $startDate, string $endDate): int|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->countNewOrdersDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Method for calculating earnings for a certain period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int|string
     */
    public function newOrdersProfit(string $startDate, string $endDate): int|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            $ordersCollection = array_column(
                $this->repo->newOrdersDuringThePeriod($startDate, $endDate)->toArray(), 'id'
            );

            return $this->subOrder->getSumAllSubOrders($ordersCollection);
        } catch (DateReportException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Method for returning approve orders
     *
     * @return \Illuminate\Support\Collection
     */
    public function getActiveOrders(): Collection
    {
        return $this->repo->getActiveOrders();
    }

    /**
     * Method for sorting the column data of an orders table for the passed keyword
     *
     * @param string $column
     * @param string $keyword
     * @param int $userId
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function orderByOrdersByOrderData(string $column, string $keyword): LengthAwarePaginator|string
    {
        $keyword = strtolower($keyword);

        if (
            (empty($column) || empty($keyword))
            || !in_array($keyword, ["asc", "desc"])
            || !in_array($column, $this->columnForSorting)
        ) {

            return 'Sorting error';
        }

        return $this->repo->orderByOrdersByOrderData($column, $keyword);
    }
}
