<?php

namespace App\Services;

use App\Exceptions\DateReportException;
use App\Repositories\CarRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class CarService extends BaseService
{
    /**
     * Property that stores columns for sorting cars
     *
     * @var array|string[]
     */
    public array $columnForSorting = ['id', 'brand', 'model', 'type', 'box_type', 'fuel_type', 'price_per_day'];

    /**
     * @param \App\Repositories\CarRepository $repo
     */
    public function __construct(CarRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Method for returning collection of new cars who added in a certain period of time with pagination
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function newCarsDuringThePeriodWithPagination(string $startDate, string $endDate): LengthAwarePaginator|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newCarsDuringThePeriodWithPagination($startDate, $endDate);
        } catch (DateReportException $e) {

            return $e->getMessage();
        }
    }

        /**
     * Method for returning collection of new cars who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Collection|string
     */
    public function newCarsDuringThePeriod(string $startDate, string $endDate): Collection|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newCarsDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {

            return $e->getMessage();
        }
    }

    /**
     * Method for returning the number of new cars for the transferred period
     *
     * @param string $startDate
     * @param string $endDate
     * @return int| string
     */
    public function countNewCarsDuringThePeriod(string $startDate, string $endDate): int|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->countNewCarsDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {

            return $e->getMessage();
        }
    }

    /**
     * Method for sorting data on cars index page
     *
     * @param string $column
     * @param string $keyword
     * @return \Illuminate\Pagination\LengthAwarePaginator|string
     */
    public function orderByCarsData(string $column, string $keyword): LengthAwarePaginator|string
    {
        $keyword = strtolower($keyword);

        if (
            (empty($column) || empty($keyword))
            || !in_array($keyword, ["asc", "desc"])
            || !in_array($column, $this->columnForSorting)
        ) {

            return 'Sorting error';
        }

        return $this->repo->orderByCarsData($column, $keyword);
    }
}
