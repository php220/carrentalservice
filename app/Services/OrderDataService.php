<?php

namespace App\Services;

use Carbon\Carbon;
use App\Services\BaseService;
use Illuminate\Support\Collection;
use App\Exceptions\DateReportException;
use App\Repositories\OrderDataRepository;

class OrderDataService extends BaseService
{
    /**
     * @param \App\Repositories\OrderDataRepository $repo
     */
    public function __construct(OrderDataRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Method for returning collection of new orders data who added in a certain period of time
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Support\Collection|string
     */
    public function newOrdersDataDuringThePeriod(string $startDate, string $endDate): Collection|string
    {
        try {
            if (
                (empty($startDate) || empty($endDate))
                || ($startDate > Carbon::now() || $endDate > Carbon::now())
                || $startDate > $endDate
            ) {

                throw new DateReportException();
            }

            return $this->repo->newOrdersDataDuringThePeriod($startDate, $endDate);
        } catch (DateReportException $e) {

            return $e->getMessage();
        }
    }

    /**
     * Method for calculated sum sub orders
     *
     * @param array $data
     * @return integer
     */
    public function getSumAllSubOrders(array $data): int
    {
        $result = 0;

        foreach ($data as $orderId) {
            $subOrderCollection = $this->repo->getSubOrdersByOrderId($orderId)->toArray();

            $result += array_sum(array_column($subOrderCollection, 'sum'));
        }

        return $result;
    }
}
