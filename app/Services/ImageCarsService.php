<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ImageCarsRepository;
use Illuminate\Database\Eloquent\Collection;

class ImageCarsService extends BaseService
{
    /**
     * @param \App\Repositories\ImageCarsRepository $repo
     */
    public function __construct(ImageCarsRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Methods for getting car images
     *
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCarImages(int $id): Collection
    {
        return $this->repo->getCarImages($id);
    }

    /**
     * Method for deleting car images
     *
     * @param integer $id
     * @param array $data
     * @return void
     */
    public function deleteCarImages(int $id, array $data)
    {
        foreach ($data as $image) {
            $this->repo->deleteCarImages($id, $image['image']);
        }
    }
}
