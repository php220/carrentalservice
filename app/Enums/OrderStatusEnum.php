<?php

namespace App\Enums;

enum OrderStatusEnum:string {
    case New = 'new';
    case Approve = 'approve';
    case Cancel = 'cancel';
    case Completed = 'completed';
    case Paid = 'paid';

    public static function values(): array
    {
        return array_column(self::cases(), 'value', 'name');
    }
}
